<script>
    $( document ).ready(function() {
        $('#navigation li.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });

        var stickyNavTop = $('#navigation').offset().top;

        var stickyNav = function () {
            var scrollTop = $(window).scrollTop();

            if (scrollTop > stickyNavTop) {
                $('#navigation').addClass('sticky');
            } else {
                $('#navigation').removeClass('sticky');
            }
        };

        stickyNav();

        $(window).scroll(function () {
            stickyNav();
        });
    });
</script>

<div class="container">
    <div id="navigation" class="animated fadeIn col-xs-12">
        <a href="{{ url('/') }}" id="logo">
            <span>4</span>Bytes
        </a>
        <div id="menu">
            <ul>
                <li>
                    <a class="active" href="{{ url('/') }}">Home</a>
                </li>
                <li class="dropdown">
                    <a href="{{ url('#') }}" class="dropdown-toggle" data-toggle="dropdown">Popups <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/popups') }}">Default</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ url('#') }}" class="dropdown-toggle" data-toggle="dropdown">Buttons <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/buttons') }}">Default</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>