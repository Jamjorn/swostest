<!DOCTYPE html>
<html>

<head>

    <!-- Mobile Ready -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Oswald|Roboto" rel="stylesheet">

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/style.css')}}">
    <link rel="stylesheet" href="{{url('css/animate.css')}}">
    <link rel="stylesheet" href="{{url('css/buttonsAnimate.css')}}">
    <link rel="stylesheet" href="{{url('css/font-awesome.min.css')}}">

    <!-- JS Files -->
    <script type="text/javascript" src="{{url('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>

</head>

<body>
    @yield('conditional')

    @yield('header')

    @yield('news')

    @yield('sub-header')

    @yield('content')

    @yield('form-content')

    @yield('footer')
</body>

</html>