@extends('app')

@section('conditional')
    <script>
        $( document ).ready(function() {
            // if(localStorage.getItem('popup') != 'shown') { // show pop-up only once per browser

                $("#close, #overlay").click(function () {
                    $(".default-pop-up, #overlay").css('z-index', -1);
                });

                var triggered = false;

                $(window).on('scroll', function () {
                    var y_scroll_pos = window.pageYOffset;
                    var scroll_pos_test = 300; // Value to determine when to trigger the pop-up

                    if (y_scroll_pos > scroll_pos_test && triggered == false) {
                        triggered = true;
                        $('.default-pop-up').addClass('animated fadeIn');

                        // localStorage.setItem('popup','shown'); // show pop-up only once per browser
                        $(".default-pop-up").css('z-index', 10001);
                        $("#overlay").css('z-index', 10000);
                    }
                });

            // }
        });
    </script>
@endsection

@section('header')
    @include('partials.navigation')
    <div class="divider"></div>
@endsection

@section('content')
    <!--
        All Pop-ups are generated on desktop screens only for usability reasons
        For this reason Pop-ups will also not be generated on screen sizes under 800px

        @Devices: Desktops
        @Range: 800px >
    -->

    <div id="overlay"></div>
    <div class="default-pop-up col-xs-6 col-sm-6 col-md-5 col-lg-4">
        <div class="row">
            <div class="image col-xs-5 col" style="background-image: url('http://www.musicscool.dk/wp-content/uploads/2017/04/DSC_2613.jpg')">
                <span id="close" class="fa fa-close"></span>
            </div>
            <div class="col-xs-7 no-padding col">
                <h3>Default Pop-up</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                <ul class="checklist">
                    <li>
                        <span class="fa fa-check"></span>
                        <span class="text">This is item #1</span>
                    </li>
                    <li>
                        <span class="fa fa-check"></span>
                        <span class="text">This is item #2</span>
                    </li>
                    <li>
                        <span class="fa fa-check"></span>
                        <span class="text">This is item #3</span>
                    </li>
                </ul>

                <div class="form-wrapper col-xs-12">
                    <form>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i></span>
                            <input type="text" class="form-control input-lg" name="name" id="name"  placeholder="Enter your Name"/>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i></span>
                            <input type="text" class="form-control input-lg" name="email" id="email"  placeholder="Enter your Email"/>
                        </div>
                        <div class="btn btn-lg col-xs-12">Subscribe Now!</div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- NOT REQUIRED -->
        <div class="scroll-simulation col-xs-12"></div>
    <!-- END NOT REQUIRED -->
@endsection