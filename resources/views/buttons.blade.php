@extends('app')

@section('conditional')

@endsection

@section('header')
    @include('partials.navigation')
    <div class="divider"></div>
@endsection

@section('content')
    <div id="effects" class="col-xs-10 col-xs-push-1 effects">

        <h2>2D Transitions</h2>

        <a href="#" class="effect-grow">Grow</a>
        <a href="#" class="effect-shrink">Shrink</a>
        <a href="#" class="effect-pulse">Pulse</a>
        <a href="#" class="effect-pulse-grow">Pulse Grow</a>
        <a href="#" class="effect-pulse-shrink">Pulse Shrink</a>
        <a href="#" class="effect-push">Push</a>
        <a href="#" class="effect-pop">Pop</a>
        <a href="#" class="effect-bounce-in">Bounce In</a>
        <a href="#" class="effect-bounce-out">Bounce Out</a>
        <a href="#" class="effect-rotate">Rotate</a>
        <a href="#" class="effect-grow-rotate">Grow Rotate</a>
        <a href="#" class="effect-float">Float</a>
        <a href="#" class="effect-sink">Sink</a>
        <a href="#" class="effect-bob">Bob</a>
        <a href="#" class="effect-hang">Hang</a>
        <a href="#" class="effect-skew">Skew</a>
        <a href="#" class="effect-skew-forward">Skew Forward</a>
        <a href="#" class="effect-skew-backward">Skew Backward</a>
        <a href="#" class="effect-wobble-horizontal">Wobble Horizontal</a>
        <a href="#" class="effect-wobble-vertical">Wobble Vertical</a>
        <a href="#" class="effect-wobble-to-bottom-right">Wobble To Bottom Right</a>
        <a href="#" class="effect-wobble-to-top-right">Wobble To Top Right</a>
        <a href="#" class="effect-wobble-top">Wobble Top</a>
        <a href="#" class="effect-wobble-bottom">Wobble Bottom</a>
        <a href="#" class="effect-wobble-skew">Wobble Skew</a>
        <a href="#" class="effect-buzz">Buzz</a>
        <a href="#" class="effect-buzz-out">Buzz Out</a>
        <a href="#" class="effect-forward">Forward</a>
        <a href="#" class="effect-backward">Backward</a>


        <h2>Background Transitions</h2>

        <a href="#" class="effect-fade">Fade</a>
        <a href="#" class="effect-back-pulse">Back Pulse</a>
        <a href="#" class="effect-sweep-to-right">Sweep To Right</a>
        <a href="#" class="effect-sweep-to-left">Sweep To Left</a>
        <a href="#" class="effect-sweep-to-bottom">Sweep To Bottom</a>
        <a href="#" class="effect-sweep-to-top">Sweep To Top</a>
        <a href="#" class="effect-bounce-to-right">Bounce To Right</a>
        <a href="#" class="effect-bounce-to-left">Bounce To Left</a>
        <a href="#" class="effect-bounce-to-bottom">Bounce To Bottom</a>
        <a href="#" class="effect-bounce-to-top">Bounce To Top</a>
        <a href="#" class="effect-radial-out">Radial Out</a>
        <a href="#" class="effect-radial-in">Radial In</a>
        <a href="#" class="effect-rectangle-in">Rectangle In</a>
        <a href="#" class="effect-rectangle-out">Rectangle Out</a>
        <a href="#" class="effect-shutter-in-horizontal">Shutter In Horizontal</a>
        <a href="#" class="effect-shutter-out-horizontal">Shutter Out Horizontal</a>
        <a href="#" class="effect-shutter-in-vertical">Shutter In Vertical</a>
        <a href="#" class="effect-shutter-out-vertical">Shutter Out Vertical</a>


        <h2>Icons</h2>

        <a href="#" class="effect-icon-back">Icon Back</a>
        <a href="#" class="effect-icon-forward">Icon Forward</a>
        <a href="#" class="effect-icon-down">Icon Down</a>
        <a href="#" class="effect-icon-up">Icon Up</a>
        <a href="#" class="effect-icon-spin">Icon Spin</a>
        <a href="#" class="effect-icon-drop">Icon Drop</a>
        <a href="#" class="effect-icon-fade">Icon Fade</a>
        <a href="#" class="effect-icon-float-away">Icon Float Away</a>
        <a href="#" class="effect-icon-sink-away">Icon Sink Away</a>
        <a href="#" class="effect-icon-grow">Icon Grow</a>
        <a href="#" class="effect-icon-shrink">Icon Shrink</a>
        <a href="#" class="effect-icon-pulse">Icon Pulse</a>
        <a href="#" class="effect-icon-pulse-grow">Icon Pulse Grow</a>
        <a href="#" class="effect-icon-pulse-shrink">Icon Pulse Shrink</a>
        <a href="#" class="effect-icon-push">Icon Push</a>
        <a href="#" class="effect-icon-pop">Icon Pop</a>
        <a href="#" class="effect-icon-bounce">Icon Bounce</a>
        <a href="#" class="effect-icon-rotate">Icon Rotate</a>
        <a href="#" class="effect-icon-grow-rotate">Icon Grow Rotate</a>
        <a href="#" class="effect-icon-float">Icon Float</a>
        <a href="#" class="effect-icon-sink">Icon Sink</a>
        <a href="#" class="effect-icon-bob">Icon Bob</a>
        <a href="#" class="effect-icon-hang">Icon Hang</a>
        <a href="#" class="effect-icon-wobble-horizontal">Icon Wobble Horizontal</a>
        <a href="#" class="effect-icon-wobble-vertical">Icon Wobble Vertical</a>
        <a href="#" class="effect-icon-buzz">Icon Buzz</a>
        <a href="#" class="effect-icon-buzz-out">Icon Buzz Out</a>


        <h2>Border Transitions</h2>

        <a href="#" class="effect-border-fade">Border Fade</a>
        <a href="#" class="effect-hollow">Hollow</a>
        <a href="#" class="effect-trim">Trim</a>
        <a href="#" class="effect-ripple-out">Ripple Out</a>
        <a href="#" class="effect-ripple-in">Ripple In</a>
        <a href="#" class="effect-outline-out">Outline Out</a>
        <a href="#" class="effect-outline-in">Outline In</a>
        <a href="#" class="effect-round-corners">Round Corners</a>
        <a href="#" class="effect-underline-from-left">Underline From Left</a>
        <a href="#" class="effect-underline-from-center">Underline From Center</a>
        <a href="#" class="effect-underline-from-right">Underline From Right</a>
        <a href="#" class="effect-reveal">Reveal</a>
        <a href="#" class="effect-underline-reveal">Underline Reveal</a>
        <a href="#" class="effect-overline-reveal">Overline Reveal</a>
        <a href="#" class="effect-overline-from-left">Overline From Left</a>
        <a href="#" class="effect-overline-from-center">Overline From Center</a>
        <a href="#" class="effect-overline-from-right">Overline From Right</a>


        <h2>Shadow and Glow Transitions</h2>

        <a href="#" class="effect-shadow">Shadow</a>
        <a href="#" class="effect-grow-shadow">Grow Shadow</a>
        <a href="#" class="effect-float-shadow">Float Shadow</a>
        <a href="#" class="effect-glow">Glow</a>
        <a href="#" class="effect-shadow-radial">Shadow Radial</a>
        <a href="#" class="effect-box-shadow-outset">Box Shadow Outset</a>
        <a href="#" class="effect-box-shadow-inset">Box Shadow Inset</a>



    </div>
    <!-- NOT REQUIRED -->
        <div class="scroll-simulation col-xs-12"></div>
    <!-- END NOT REQUIRED -->
@endsection