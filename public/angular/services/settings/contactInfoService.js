angular.module('contactInfoService', [])

    .factory('ContactInfo', function($http) {

        var model = 'settings/contactInfo';

        return {
            get : function($id){
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/get/'+$id+''
                });
            },
            getAll : function() {
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/getAll/'
                });
            },
            getAllWithPagination : function($page) {
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/getAllWithPagination/',
                    params: {page:  $page}
                });
            },
            getAllMainCategories : function() {
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/getAllMainCategories/'
                });
            },
            delete : function($id){
                return $http({
                    method: 'POST',
                    url: '/admin/'+model+'/delete/'+$id+''
                });
            },
            search : function($search_value) {
                return $http({
                    method: 'POST',
                    url: '/admin/'+model+'/search/' + $search_value
                });
            }
        }

    });