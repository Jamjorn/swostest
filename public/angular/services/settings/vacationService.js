angular.module('vacationService', [])

    .factory('Vacations', function($http) {

        var model = 'settings/vacations';

        return {
            get : function($id){
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/get/'+$id+''
                });
            },
            getAll : function() {
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/getAll/'
                });
            },
            getAllWithPagination : function($page) {
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/getAllWithPagination/',
                    params: {page:  $page}
                });
            },
            getAllMainCategories : function() {
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/getAllMainCategories/'
                });
            },
            delete : function($id){
                return $http({
                    method: 'POST',
                    url: '/admin/'+model+'/delete/'+$id+''
                });
            },
            search : function($search_value) {
                return $http({
                    method: 'POST',
                    url: '/admin/'+model+'/search/' + $search_value
                });
            },
            addObject : function($object) {
                return $http({
                    method: 'POST',
                    url: '/admin/'+model+'/addObject/',
                    data: $object
                });
            },
            editObject : function($object, $id) {
                return $http({
                    method: 'POST',
                    url: '/admin/'+model+'/editObject/'+$id,
                    data: $object
                });
            },
            getTimeschedules : function(){
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/getTimeschedules/'
                });
            },
            getTimeschedulesWithVacations : function($page){
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/getTimeschedulesWithVacations/',
                    params: {page:  $page}
                });
            }
        }

    });