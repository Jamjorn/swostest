angular.module('facturatieRouteManagerService', [])

    .factory('FacturatieRouteManager', function($http) {

        var model = 'route_manager';

        return {
            generateRoutes : function($week_nr, $year){
                return $http({
                    method: 'POST',
                    url: '/facturatie/'+model+'/generateRoutes',
                    data: {weeknr: $week_nr, year: $year}
                });
            },
            checkRoutesAreGenerated : function($week_nr, $year, $all){
                return $http({
                    method: 'GET',
                    url: '/facturatie/'+model+'/checkRoutesAreGenerated/'+$week_nr+'/'+$year+'/'+$all
                });
            },
            loadRouteData : function($day, $week_nr, $year){
                return $http({
                    method: 'GET',
                    url: '/facturatie/'+model+'/loadRouteData/'+$day+'/'+$week_nr+'/'+$year
                });
            },
            changeStatus : function($route_manager_id, $status){
                return $http({
                    method: 'POST',
                    url: '/facturatie/'+model+'/changeStatus',
                    data: {route_manager_id: $route_manager_id, status: $status}
                });
            }
        }

    });