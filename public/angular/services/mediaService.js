angular.module('mediaService', [])

    .factory('Media', function($http) {

        return {
            getSingleImage : function($id){
                return $http({ method: 'GET',
                               url: '/admin/mediatheek/loadSingleImage/'+$id+''
                });
            },
            getAll : function() {
                return $http({ method: 'GET',
                               url: '/admin/api/mediatheek/getAll/'
                });
            },
            update : function($id, $name, $alt, $path) {
                return $http({ method: 'POST',
                               url: '/admin/api/mediatheek/update/'+$id+'/'+$name+'/'+$alt+''
                });
            },
            delete : function($name){
                return $http({ method: 'POST',
                               url: '/admin/dropzone/delete/'+$name+''
                });
            },
            deleteOnPopup : function($id){
                return $http({ method: 'POST',
                               url: '/admin/api/mediatheek/delete/'+$id+''
                });
            },
            search : function($search_value) {
                return $http({ method: 'POST',
                               url: '/admin/mediatheek/search/' + $search_value
                });
            }
        }

    });