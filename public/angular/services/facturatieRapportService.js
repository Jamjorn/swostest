angular.module('facturatieRapportService', [])

    .factory('FacturatieRapports', function($http) {

        var model = 'rapports';

        return {
            get: function ($month, $year) {
                return $http({
                    method: 'GET',
                    url: '/facturatie/' + model + '/get/' + $month + '/' + $year
                });
            },
            generateJaarOmzetGraph: function ($year) {
                return $http({
                    method: 'GET',
                    url: '/facturatie/' + model + '/generateJaarOmzetGraph/' + $year
                });
            },
            generateVastVsLosPie: function ($year) {
                return $http({
                    method: 'GET',
                    url: '/facturatie/' + model + '/generateVastVsLosPie/' + $year
                });
            },
            generateLocationMaandOmzetPie: function ($year) {
                return $http({
                    method: 'GET',
                    url: '/facturatie/' + model + '/generateLocationMaandOmzetPie/' + $year
                });
            },
            generateTotalOrderperDayPie: function ($year) {
                return $http({
                    method: 'GET',
                    url: '/facturatie/' + model + '/generateTotalOrderperDayPie/' + $year
                });
            },
            generateAfhalenVsBezorgenMaandOmzetPie: function ($year) {
                return $http({
                    method: 'GET',
                    url: '/facturatie/' + model + '/generateAfhalenVsBezorgenMaandOmzetPie/' + $year
                });
            },
            generateMostBoughtProductsChart: function ($year) {
                return $http({
                    method: 'GET',
                    url: '/facturatie/' + model + '/generateMostBoughtProductsChart/' + $year
                });
            },
            generateTotalCustomers: function () {
                return $http({
                    method: 'GET',
                    url: '/facturatie/' + model + '/generateTotalCustomers/'
                });
            }
        }

    });