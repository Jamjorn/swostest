angular.module('facturatieCategoryService', [])

    .factory('FacturatieCategory', function($http) {

        var model = 'category';

        return {
            get : function($id){
                return $http({
                    method: 'GET',
                    url: '/facturatie/'+model+'/get/'+$id+''
                });
            },
            getAll : function() {
                return $http({
                    method: 'GET',
                    url: '/facturatie/'+model+'/getAll/'
                });
            },
            getAllWithPagination : function($page) {
                return $http({
                    method: 'GET',
                    url: '/facturatie/'+model+'/getAllWithPagination/',
                    params: {page:  $page}
                });
            },
            getAllMainCategories : function() {
                return $http({
                    method: 'GET',
                    url: '/facturatie/'+model+'/getAllMainCategories/'
                });
            },
            delete : function($id){
                return $http({
                    method: 'POST',
                    url: '/facturatie/'+model+'/delete/'+$id+''
                });
            },
            search : function($search_value) {
                return $http({
                    method: 'POST',
                    url: '/facturatie/'+model+'/search/' + $search_value
                });
            },
            addObject : function($object) {
                return $http({
                    method: 'POST',
                    url: '/facturatie/'+model+'/addObject',
                    data: $object
                });
            },
            editObject : function($object, $id) {
                return $http({
                    method: 'POST',
                    url: '/facturatie/'+model+'/editObject/'+$id,
                    data: $object
                });
            }
        }

    });