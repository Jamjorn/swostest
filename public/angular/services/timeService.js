angular.module('timeService', [])

    .factory('Times', function($http) {

        return {

            // get all the times
            get : function(location, type) {
                return $http.get('time/get/'+location+'/'+type+'');
            },
            // get customer for order
            getCustomer : function() {
                return $http.get('time/getCustomer');
            }
        }

    });
