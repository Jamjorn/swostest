angular.module('productService', [])

.factory('Products', function($http) {

    var model = 'product';

    return {
        get : function($id){
            return $http({
                method: 'GET',
                url: '/admin/'+model+'/get/'+$id+''
            });
        },
        getAll : function() {
            return $http({
                method: 'GET',
                url: '/admin/'+model+'/getAll/'
            });
        },
        getAllWithPagination : function($page) {
            return $http({
                method: 'GET',
                url: '/admin/'+model+'/getAllWithPagination/',
                params: {page:  $page}
            });
        },
        delete : function($id){
            return $http({
                method: 'POST',
                url: '/admin/'+model+'/delete/'+$id+''
            });
        },
        search : function($search_value) {
            return $http({
                method: 'POST',
                url: '/admin/'+model+'/search/' + $search_value
            });
        },
        addObject : function($object) {
            return $http({
                method: 'POST',
                url: '/admin/'+model+'/addObject/',
                data: $object
            });
        },
        editObject : function($object, $id) {
            return $http({
                method: 'POST',
                url: '/admin/'+model+'/editObject/'+$id,
                data: $object
            });
        },
        getInWebshop : function($id){
            return $http({
                method: 'GET',
                url: '/'+model+'/getInWebshop/'+$id
            });
        },
        getAllInWebshop : function($category) {
            return $http({
                method: 'GET',
                url: '/'+model+'/getAllInWebshop/'+$category
            });
        },
        searchInWebshop : function($input){
            return $http({
                method: 'POST',
                url: '/'+model+'/searchInWebshop/'+$input
            });
        }

    }

});