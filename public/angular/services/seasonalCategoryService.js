angular.module('seasonalCategoryService', [])

    .factory('seasonalCategories', function($http) {

        var model = 'category';

        return {
            get : function($id){
                return $http({
                    method: 'GET',
                    url: '/admin/seasonal/'+model+'/get/'+$id+''
                });
            },
            getSeason : function($id){
                return $http({
                    method: 'GET',
                    url: '/admin/seasonal/'+model+'/getseason/'+$id+''
                });
            },
            getAll : function() {
                return $http({
                    method: 'GET',
                    url: '/admin/seasonal/'+model+'/getAll/'
                });
            },
            getAllWithPagination : function($page) {
                return $http({
                    method: 'GET',
                    url: '/admin/seasonal/'+model+'/getAllWithPagination/',
                    params: {page:  $page}
                });
            },
            getAllMainCategories : function() {
                return $http({
                    method: 'GET',
                    url: '/admin/seasonal/'+model+'/getAllMainCategories/'
                });
            },
            getAllSeasons : function() {
                return $http({
                    method: 'GET',
                    url: '/admin/seasonal/'+model+'/getAllSeasons/'
                });
            },
            delete : function($id){
                return $http({
                    method: 'POST',
                    url: '/admin/seasonal/'+model+'/delete/'+$id+''
                });
            },
            search : function($search_value) {
                return $http({
                    method: 'POST',
                    url: '/admin/seasonal/'+model+'/search/' + $search_value
                });
            },
            addObject : function($object) {
                return $http({
                    method: 'POST',
                    url: '/admin/seasonal/'+model+'/addObject/',
                    data: $object
                });
            },
            editObject : function($object, $id) {
                return $http({
                    method: 'POST',
                    url: '/admin/seasonal/'+model+'/editObject/'+$id,
                    data: $object
                });
            }
        }

    });