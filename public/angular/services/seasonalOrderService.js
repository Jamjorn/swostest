angular.module('seasonalOrderService', [])

    .factory('SeasonalOrder', function($http) {

        var model = 'order';

        return {
            get : function($id){
                return $http({
                    method: 'GET',
                    url: '/admin/seasonal/'+model+'/get/'+$id+''
                });
            },
            getAll : function() {
                return $http({
                    method: 'GET',
                    url: '/admin/seasonal/'+model+'/getAll/'
                });
            },
            getAllWithPagination : function($page) {
                return $http({
                    method: 'GET',
                    url: '/admin/seasonal/'+model+'/getAllWithPagination/',
                    params: {page:  $page}
                });
            },
            delete : function($id){
                return $http({
                    method: 'POST',
                    url: '/admin/seasonal/'+model+'/delete/'+$id+''
                });
            },
            search : function($search_value) {
                return $http({
                    method: 'POST',
                    url: '/admin/seasonal/'+model+'/search/' + $search_value
                });
            }
        }

    });