angular.module('cartService', [])

    .factory('Cart', function($http) {

        var model = 'cart';

        return {
            addToCart : function ($object) {
                return $http({
                    method: 'POST',
                    url: '/' + model + '/addToCart/',
                    data: $object
                });
            },
            changeQuantity : function ($object) {
                return $http({
                    method: 'POST',
                    url: '/' + model + '/changeQuantity/',
                    data: $object
                });
            },
            removeSingleItemFromCart : function ($object) {
                return $http({
                    method: 'POST',
                    url: '/' + model + '/removeSingleItemFromCart/',
                    data: $object
                });
            },
            removeFromCart : function ($object) {
                return $http({
                    method: 'POST',
                    url: '/' + model + '/removeFromCart/',
                    data: $object
                });
            },
            getCart : function () {
                return $http({
                    method: 'GET',
                    url: '/' + model + '/getCart/'
                });
            },
            debug : function () {
                return $http({
                    method: 'GET',
                    url: '/' + model + '/debug/'
                });
            }
        }

    });
