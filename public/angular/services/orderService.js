angular.module('orderService', [])

    .factory('Order', function($http) {

        var model = 'order';

        return {
            get : function($id){
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/get/'+$id+''
                });
            },
            getAll : function() {
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/getAll/'
                });
            },
            getAllWithPagination : function($page) {
                return $http({
                    method: 'GET',
                    url: '/admin/'+model+'/getAllWithPagination/',
                    params: {page:  $page}
                });
            },
            delete : function($id){
                return $http({
                    method: 'POST',
                    url: '/admin/'+model+'/delete/'+$id+''
                });
            },
            search : function($search_value) {
                return $http({
                    method: 'POST',
                    url: '/admin/'+model+'/search/' + $search_value
                });
            }
        }

    });