angular.module('seasonalProductService', [])

.factory('Seasonalproducts', function($http) {

    var model = 'product';

    return {
        get : function($id){
            return $http({
                method: 'GET',
                url: '/admin/seasonal/'+model+'/get/'+$id+''
            });
        },
        getAll : function() {
            return $http({
                method: 'GET',
                url: '/admin/seasonal/'+model+'/getAll/'
            });
        },
        getAllWithProducts : function() {
            return $http({
                method: 'GET',
                url: '/admin/seasonal/'+model+'/getAllWithProducts/'
            });
        },
        getAllWithPagination : function($page) {
            return $http({
                method: 'GET',
                url: '/admin/seasonal/'+model+'/getAllWithPagination/',
                params: {page:  $page}
            });
        },
            getAllSeasons : function() {
                return $http({
                    method: 'GET',
                    url: '/admin/seasonal/'+model+'/getAllSeasons/'
                });
            },
        delete : function($id){
            return $http({
                method: 'POST',
                url: '/admin/seasonal/'+model+'/delete/'+$id+''
            });
        },
        search : function($search_value) {
            return $http({
                method: 'POST',
                url: '/admin/seasonal/'+model+'/search/' + $search_value
            });
        },
        addObject : function($object) {
            return $http({
                method: 'POST',
                url: '/admin/seasonal/'+model+'/addObject/',
                data: $object
            });
        },
        editObject : function($object, $id) {
            return $http({
                method: 'POST',
                url: '/admin/seasonal/'+model+'/editObject/'+$id,
                data: $object
            });
        },
        getInWebshop : function($id){
            return $http.get('product/get/'+$id+'');
        },
        getAllInWebshop : function($category) {
            return $http.get('product/getAll/'+$category+'');
        },
        searchInWebshop : function($input){
            return $http.post('product/search/'+$input+'');
        }

    }

});