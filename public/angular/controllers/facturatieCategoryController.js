angular.module('facturatieCategoryCtrl', [])

// inject the Comment service into our controller
    .controller('facturatieCategoryController', function($scope, $http, $timeout, FacturatieCategory) {

        $scope.loading = true;
        $scope.once = true;
        $scope.isSaving = false;

        $scope.object = {};
        $scope.selected_type = '';
        $scope.selected_main = '';

        $scope.mains = [];
        $scope.types = [{
            Id: 1,
            Name: 'sub'
        }, {
            Id: 2,
            Name: 'hoofd'
        }];

        /**
         * Initialize the overview page
         */
        $scope.init = function() {
            FacturatieCategory.getAllWithPagination(1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.get = function($id, $dropdown){
            FacturatieCategory.get($id)
                .then(function (success) {
                    $scope.object = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
            if($dropdown){
                $scope.initMainCategories();
            }
        };

        /**
         * Get data by pagination - 10 item per page
         * @param $page : the page you want the data from
         */
        $scope.getPage = function($page) {
            $scope.loading = true;
            FacturatieCategory.getAllWithPagination($page)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get data for the next page [the next 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         * @param $last_page : is het last page of the data set
         */
        $scope.nextPage = function($current_page, $last_page) {
            if($current_page != $last_page) {
                $scope.loading = true;
                FacturatieCategory.getAllWithPagination($current_page + 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Get data for the previous page [the previous 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         */
        $scope.previousPage = function($current_page) {
            if($current_page != 1) {
                $scope.loading = true;
                FacturatieCategory.getAllWithPagination($current_page - 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Search for data by string.
         * If data matches part of the string one or more results will be returned
         * Else if $search_value string is empty it will return the first page of the current data set
         *
         * @param $search_value : is the given input as string
         */
        $scope.search = function($search_value) {
            if($search_value != ''){
                $scope.loading = true;
                FacturatieCategory.search($search_value)
                    .then(function (success) {
                        $scope.data = success;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                FacturatieCategory.getAllWithPagination(1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Deletes the given object by id
         * Return to the current page after delete
         *
         * @param $id : id of the object
         * @param $current_page : the page the dataset is on
         */
        $scope.delete = function($id){
            $scope.loading = true;
            FacturatieCategory.delete($id)
                .then(function (success) {
                    FacturatieCategory.getAllWithPagination(success.current_page)
                        .then(function (success) {
                            $scope.data = success.data;
                        }).catch(function (e) {
                            console.log("got an error in the process", e);
                        });
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.addObject = function(){
            $scope.isSaving = true;
            $scope.validationMessage = false;

            $scope.object.type = $scope.selected_type.Name;
            if($scope.object.type == "hoofd"){
                $scope.object.main = "";
            } else if($scope.object.type == "sub"){
                $scope.object.main = $scope.selected_main.Name;
            }

            FacturatieCategory.addObject($scope.object)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een categorie aangemaakt.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        $scope.editObject = function($id){
            $scope.isSaving = true;
            $scope.validationMessage = false;

            $scope.object.type = $scope.selected_type.Name;
            if($scope.object.type == "hoofd"){
                $scope.object.main = "";
            } else if($scope.object.type == "sub"){
                $scope.object.main = $scope.selected_main.Name;
            }

            FacturatieCategory.editObject($scope.object, $id)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een categorie gewijzigd.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        /**
         * Initialize the overview page
         */
        $scope.initMainCategories = function() {
            FacturatieCategory.getAllMainCategories()
                .then(function (success) {
                    angular.forEach(success.data, function(value, key) {
                        $scope.mains.push({ Id: value.id, Name: value.name });
                    });
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.GetValueTypeDropdown = function () {
            var typeID = this.selected_type.Id;
            $scope.selected_type = $.grep($scope.types, function (selected_type) {
                return selected_type.Id == typeID;
            })[0];
            return $scope.selected_type;
        };

        $scope.GetMainDropdown = function () {
            var mainID = this.selected_main.Id;
            $scope.selected_main = $.grep($scope.mains, function (selected_main) {
                return selected_main.Id == mainID;
            })[0];
            return $scope.selected_main;
        };

        $scope.initDropdown = function($id){
            if($scope.once) {
                $scope.loading = true;
                FacturatieCategory.get($id)
                    .then(function (success) {
                        $scope.data = success.data;
                        for ($i = 0; $i < $scope.types.length; $i++) {
                            if ($scope.data.type == $scope.types[$i].Name) {
                                $scope.selected_type = $scope.types[$i];
                            }
                        }
                        for ($i = 0; $i < $scope.mains.length; $i++) {
                            if ($scope.data.main == $scope.mains[$i].Name) {
                                $scope.selected_main = $scope.mains[$i];
                            }
                        }
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
            $scope.once = false;
        };

        $scope.$watch('selected_type', function(newVal, oldVal) {
            $scope.main = false;
            if(newVal.Name == 'sub'){
                $scope.main = true;
            } else if(newVal.Name == 'hoofd'){
                $scope.main = false;
            }
        });

    });


