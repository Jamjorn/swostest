angular.module('seasonalCategoryCtrl', [])

// inject the Comment service into our controller
    .controller('seasonalCategoryController', function($scope, $http, $timeout, seasonalCategories) {

        $scope.loading = true;
        $scope.once = true;
        $scope.isSaving = false;

        $scope.object = {};
        $scope.selected_type = '';
        $scope.selected_main = '';
        $scope.selected_seasons = '';
        $scope.selected_flter = '';

        $scope.mains = [];
        $scope.seasons = [];
        $scope.filter = [];
        $scope.types = [{
            Id: 1,
            Name: 'sub'
        }, {
            Id: 2,
            Name: 'hoofd'
        }];

        /**
         * Initialize the overview page
         */
        $scope.init = function() {
            seasonalCategories.getAllWithPagination(1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.getAllSeasons();
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

         /**
         * Initialize the overview page
         */
        $scope.getAllSeasons = function() {

            seasonalCategories.getAllSeasons()
                .then(function (success) {
                    angular.forEach(success.data, function(value, key) {
                        $scope.filter.push({ Id: value.id, Name: value.name });
                    });
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });



        };

        $scope.get = function($id, $dropdown){
            seasonalCategories.get($id)
                .then(function (success) {
                    $scope.object = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
            if($dropdown){
                $scope.initMainCategories();
            }
        };

        /**
         * Get data by pagination - 10 item per page
         * @param $page : the page you want the data from
         */
        $scope.getPage = function($page) {
            $scope.loading = true;
            seasonalCategories.getAllWithPagination($page)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get data for the next page [the next 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         * @param $last_page : is het last page of the data set
         */
        $scope.nextPage = function($current_page, $last_page) {
            if($current_page != $last_page) {
                $scope.loading = true;
                seasonalCategories.getAllWithPagination($current_page + 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Get data for the previous page [the previous 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         */
        $scope.previousPage = function($current_page) {
            if($current_page != 1) {
                $scope.loading = true;
                seasonalCategories.getAllWithPagination($current_page - 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Search for data by string.
         * If data matches part of the string one or more results will be returned
         * Else if $search_value string is empty it will return the first page of the current data set
         *
         * @param $search_value : is the given input as string
         */
        $scope.search = function($search_value) {
            if($search_value != ''){
                $scope.loading = true;
                seasonalCategories.search($search_value)
                    .then(function (success) {
                        $scope.data = success;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                seasonalCategories.getAllWithPagination(1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Deletes the given object by id
         * Return to the current page after delete
         *
         * @param $id : id of the object
         * @param $current_page : the page the dataset is on
         */
        $scope.delete = function($id){
            $scope.loading = true;
            seasonalCategories.delete($id)
                .then(function (success) {
                    seasonalCategories.getAllWithPagination(success.current_page)
                        .then(function (success) {
                            $scope.data = success.data;
                        }).catch(function (e) {
                            console.log("got an error in the process", e);
                        });
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.addObject = function(){
            $scope.isSaving = true;
            $scope.validationMessage = false;


              $scope.object.season = $scope.selected_seasons.Name;

            // $scope.object.type = $scope.selected_type.Name;
            // $scope.object.season = $scope.selected_seasons.Name;
            // if($scope.object.type == "hoofd"){
            //     $scope.object.main = "";
            // } else if($scope.object.type == "sub"){
            //     $scope.object.main = $scope.selected_main.Name;
            // }


            seasonalCategories.addObject($scope.object)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een categorie aangemaakt.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        $scope.editObject = function($id){
            $scope.isSaving = true;
            $scope.validationMessage = false;

            $scope.object.type = $scope.selected_type.Name;
            $scope.object.season = $scope.selected_seasons.Name;
            if($scope.object.type == "hoofd"){
                $scope.object.main = "";
            } else if($scope.object.type == "sub"){
                $scope.object.main = $scope.selected_main.Name;
            }

            seasonalCategories.editObject($scope.object, $id)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een categorie gewijzigd.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        /**
         * Initialize the overview page
         */
        $scope.initMainCategories = function() {
            // seasonalCategories.getAllMainCategories()
            //     .then(function (success) {
            //         angular.forEach(success.data, function(value, key) {
            //             $scope.mains.push({ Id: value.id, Name: value.name });
            //         });
            //         $scope.loading = false;
            //     }).catch(function (e) {
            //         console.log("got an error in the process", e);
            //     });

            seasonalCategories.getAllSeasons()
                .then(function (success) {
                    angular.forEach(success.data, function(value, key) {
                        $scope.seasons.push({ Id: value.id, Name: value.name });
                    });
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });



        };

        $scope.GetValueTypeDropdown = function () {
            var typeID = this.selected_type.Id;
            $scope.selected_type = $.grep($scope.types, function (selected_type) {
                return selected_type.Id == typeID;
            })[0];
            return $scope.selected_type;
        };

        $scope.GetMainDropdown = function () {
            var mainID = this.selected_main.Id;
            $scope.selected_main = $.grep($scope.mains, function (selected_main) {
                return selected_main.Id == mainID;
            })[0];
            return $scope.selected_main;
        };
        $scope.GetSeasonsDropdown = function () {
            var seasonsID = this.selected_seasons.Id;
            $scope.selected_seasons = $.grep($scope.seasons, function (selected_seasons) {
                return selected_seasons.Id == seasonsID;
            })[0];
            return $scope.selected_seasons;
        };

        $scope.initDropdown = function($id){
            if($scope.once) {
                $scope.loading = true;
                seasonalCategories.getSeason($id)
                    .then(function (success) {
                        $scope.data = success.data;
                        // for ($i = 0; $i < $scope.types.length; $i++) {
                        //     if ($scope.data.type == $scope.types[$i].Name) {
                        //         $scope.selected_type = $scope.types[$i];
                        //     }
                        // }
                        // for ($i = 0; $i < $scope.mains.length; $i++) {
                        //     if ($scope.data.main == $scope.mains[$i].Name) {
                        //         $scope.selected_main = $scope.mains[$i];
                        //     }
                        // }
                        for ($i = 0; $i < $scope.seasons.length; $i++) {
                            if ($scope.data.name == $scope.seasons[$i].Name) {
                                $scope.selected_seasons = $scope.seasons[$i];
                            }
                        }
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
            $scope.once = false;
        };

        $scope.$watch('selected_type', function(newVal, oldVal) {
            $scope.main = false;
            if(newVal.Name == 'sub'){
                $scope.main = true;
            } else if(newVal.Name == 'hoofd'){
                $scope.main = false;
            }
        });

    });


