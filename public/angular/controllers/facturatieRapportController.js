angular.module('facturatieRapportCtrl', [])

.controller('facturatieRapportController', function($scope, $http, $filter, $window, $timeout, FacturatieRapports) {

    $scope.loading = true;
    $scope.once = true;
    $scope.isSaving = false;
    $scope.selected_type = '';
    $scope.selected_soort = '';

    $scope.totalOmzet = 0.00;
    $scope.object = {};
    $scope.product_list = [''];

    $scope.get = function($month, $year){
        FacturatieRapports.get($month, $year)
        .then(function (success) {
                $scope.object = success.data;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.generateGraphs = function($year){
        $scope.generateJaarOmzetGraph($year);
        $scope.generateVastVsLosPie($year);
        $scope.generateLocationMaandOmzetPie($year);
        $scope.generateTotalOrderperDayPie($year);
        $scope.generateAfhalenVsBezorgenMaandOmzetPie($year);
        //$scope.generateMostBoughtProductsChart($year);
        $scope.totalCustomers = $scope.generateTotalCustomers($year);
    };

    $scope.generateJaarOmzetGraph = function($year){
        $scope.labels = ['Januari','Februari','Maart','April','Mei','Juni','Juli','Augustus','September','Oktober','November','December'];
        $scope.series = ['Omzet per Maand'];
        $scope.data = [
            []
        ];
        $scope.datasetOverride = [
            {
                yAxisID: 'y-axis-1'
            }
        ];
        $scope.options = {
            legend: {
                display: true
            },
            title:{
                display: true,
                text: 'Jaar Omzet - (Locatie: x)'
            },
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left',
                        scaleLabel: {
                            display: true,
                            labelString: 'Omzet in €'
                        }
                    }
                ]
            }
        };

        FacturatieRapports.generateJaarOmzetGraph($year)
            .then(function (success) {
                $scope.jaarOmzetGraph = success.data;
                angular.forEach($scope.jaarOmzetGraph, function(value, key) {
                    $scope.data[0][key - 1] = value.totalprice;
                    $scope.totalOmzet = parseFloat($scope.totalOmzet, 6) + parseFloat(value.totalprice,6);
                    $scope.totalOmzet = $filter('number')($scope.totalOmzet, 2);
                });
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.generateVastVsLosPie = function($year) {
        $scope.labels1 = ["Losse Bestellingen", "Vaste Bestellingen"];
        $scope.data1 = [];
        $scope.options1 = {legend: {display: true}};

        FacturatieRapports.generateVastVsLosPie($year)
            .then(function (success) {
                var result = success.data;
                $scope.data1 = [result.los, result.vast];
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.generateLocationMaandOmzetPie = function($year){
        // @TODO
    };

    $scope.generateTotalOrderperDayPie = function($year){
        $scope.labels3 = ["Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag"];
        $scope.data3 = [];
        $scope.options3 = {legend: {display: true}};

        FacturatieRapports.generateTotalOrderperDayPie($year)
            .then(function (success) {
                var result = success.data;
                $scope.data3 = [result.maandag,
                                result.dinsdag,
                                result.woensdag,
                                result.donderdag,
                                result.vrijdag,
                                result.zaterdag];
                $scope.totalOrders = result.maandag + result.dinsdag + result.woensdag + result.donderdag + result.vrijdag + result.zaterdag;
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.generateAfhalenVsBezorgenMaandOmzetPie = function($year){
        $scope.labels4 = ["Afhalen", "Bezorgen"];
        $scope.data4 = [];
        $scope.options4 = {legend: {display: true}};

        FacturatieRapports.generateAfhalenVsBezorgenMaandOmzetPie($year)
            .then(function (success) {
                var result = success.data;
                $scope.data4 = [result.afhalen, result.bezorgen];
                $scope.loading = false;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

    $scope.generateTotalCustomers = function(){
        FacturatieRapports.generateTotalCustomers()
            .then(function (success) {
                $scope.totalCustomers = success.data;
            }).catch(function (e) {
                console.log("got an error in the process", e);
            });
    };

});

