angular.module('adminProductCtrl', [])

    .controller('adminProductController', function($scope) {

        $scope.textProductNaamLength = 50;
        $scope.types = [{
            Id: 1,
            Name: 'Standaard'
        }, {
            Id: 2,
            Name: 'Brood'
        }, {
            Id: 3,
            Name: 'Taart'
        }];


        $scope.$watch('naam', function (newValue, oldValue) {
            if (newValue) {
                if (newValue.length > 50) {
                    $scope.naam = oldValue;
                }
                $scope.textProductNaamLength = 50 - newValue.length;
            }
        });

        $scope.updateBody = function (event) {
            return false;
        };

        $scope.GetValueTypeDropdown = function () {
            var typeID = this.selected_type.Id;
            $scope.the_type = $.grep($scope.types, function (selected_type) {
                return selected_type.Id == typeID;
            })[0].Name;
            return $scope.the_type;
        };

  

        $scope.$watch('naam', function(newVal, oldVal) {
            if(newVal){
                $scope.product_name_brood = newVal + ' (half)';
                $scope.product_name_brood_heel = newVal + ' (heel)';
            } else{
                $scope.product_name_brood = '';
                $scope.product_name_brood_heel = '';
            }
        });

    });



