angular.module('facturatieCustomerCtrl', [])

// inject the Comment service into our controller
    .controller('facturatieCustomerController', function($scope, $http, $timeout, FacturatieCustomer) {

        $scope.loading = true;
        $scope.once = true;
        $scope.isSaving = false;

        $scope.object = {};

        /**
         * Initialize the overview page
         */
        $scope.init = function() {
            FacturatieCustomer.getAllWithPagination(1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.get = function($id){
            FacturatieCustomer.get($id)
                .then(function (success) {
                    $scope.object = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get data by pagination - 10 item per page
         * @param $page : the page you want the data from
         */
        $scope.getPage = function($page) {
            $scope.loading = true;
            FacturatieCustomer.getAllWithPagination($page)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get data for the next page [the next 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         * @param $last_page : is het last page of the data set
         */
        $scope.nextPage = function($current_page, $last_page) {
            if($current_page != $last_page) {
                $scope.loading = true;
                FacturatieCustomer.getAllWithPagination($current_page + 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Get data for the previous page [the previous 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         */
        $scope.previousPage = function($current_page) {
            if($current_page != 1) {
                $scope.loading = true;
                FacturatieCustomer.getAllWithPagination($current_page - 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Search for data by string.
         * If data matches part of the string one or more results will be returned
         * Else if $search_value string is empty it will return the first page of the current data set
         *
         * @param $search_value : is the given input as string
         */
        $scope.search = function($search_value) {
            if($search_value != ''){
                $scope.loading = true;
                FacturatieCustomer.search($search_value)
                    .then(function (success) {
                        $scope.data = success;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                FacturatieCustomer.getAllWithPagination(1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Deletes the given object by id
         * Return to the current page after delete
         *
         * @param $id : id of the object
         * @param $current_page : the page the dataset is on
         */
        $scope.delete = function($id){
            $scope.loading = true;
            FacturatieCustomer.delete($id)
                .then(function (success) {
                    FacturatieCustomer.getAllWithPagination(success.current_page)
                        .then(function (success) {
                            $scope.data = success.data;
                        }).catch(function (e) {
                            console.log("got an error in the process", e);
                        });
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.addObject = function(){
            $scope.isSaving = true;
            $scope.validationMessage = false;

            FacturatieCustomer.addObject($scope.object)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een klant aangemaakt.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        $scope.editObject = function($id){
            $scope.isSaving = true;
            $scope.validationMessage = false;

            FacturatieCustomer.editObject($scope.object, $id)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een klant gewijzigd.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

    });


