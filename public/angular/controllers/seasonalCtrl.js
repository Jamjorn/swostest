angular.module('seasonalCtrl', [])

// inject the Comment service into our controller
    .controller('seasonalController', function($scope, $http) {

        $scope.textProductNaamLength = 50;
        $scope.items = [];
        $scope.lastpage=1;
        $scope.path="";
        $scope.thelastpage = 0;
        $scope.kerst_products = '';
        $scope.deleteURL = '/admin/seasonal/delete/';
        $scope.loading = true;

        $scope.$watch('naam', function (newValue, oldValue) {
            if (newValue) {
                if (newValue.length > 50) {
                    $scope.naam = oldValue;
                }
                $scope.textProductNaamLength = 50 - newValue.length;
            }
        });
         $scope.types = [{
            Id: 1,
            Name: 'Standaard'
        }, {
            Id: 2,
            Name: 'Brood'
        }, {
            Id: 3,
            Name: 'Taart'
        }];

        $scope.updateBody = function (event) {
            return false;
        };

        $scope.init =function(path) {
            $scope.path=path;
            $scope.lastpage=1;
            $http({
                url: 'api/seasonal/'+ path,
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function(data, status, headers, config) {
                $scope.thelastpage = data.last_page;
                $scope.data = data.data;
                $scope.currentpage = data.current_page;
                $scope.loading = false;
            });
        };
        //$scope.init(); -> out commited -> do not need this for every page that calls the controller

        $scope.getFirstPage = function() {
            $scope.loading = true;
            $scope.lastpage = 1;
            $http({
                url: 'api/seasonal/'+$scope.path,
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function (data, status, headers, config) {
                $scope.kerst_products = data.data;
                $scope.loading = false;
            });
        };

        $scope.getLastPage = function() {
            $scope.loading = true;
            $scope.lastpage = $scope.thelastpage;
            $http({
                url: 'api/seasonal/'+$scope.path,
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function (data, status, headers, config) {
                $scope.kerst_products = data.data;
                $scope.loading = false;
            });
        };

        $scope.nextPage = function() {
            $scope.loading = true;
            $scope.lastpage +=1;
            $http({
                url: 'api/seasonal/'+$scope.path,
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function (data, status, headers, config) {
                $scope.kerst_products = data.data;
                $scope.loading = false;
            });
        };

        $scope.previousPage = function() {
            $scope.loading = true;
            if($scope.lastpage == 1)
            {
                $scope.lastpage=1;
            } else {
                $scope.lastpage -=1;
            }
            $http({
                url: 'api/seasonal/'+$scope.path,
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function (data, status, headers, config) {
                $scope.kerst_products = data.data;
                $scope.loading = false;
            });
        };

        $scope.reloadKerstItems = function() {
            $scope.loading = true;
            $http({
                url: 'api/seasonal/'+$scope.path,
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function (data, status, headers, config) {
                $scope.kerst_products = data.data;
                $scope.loading = false;
            });
        };

        $scope.deleteKerstItems = function($id) {
            $scope.loading = true;
            $http({
                method  : 'POST',
                url     : 'seasonal/delete/' + $id
            }).then(function(data) {
                $scope.reloadKerstItems();
                $scope.loading = false;
            });

        };

        $scope.searchKerstItems = function() {
            if($scope.search_value != ''){
                $scope.loading = true;
                $http({
                    method  : 'POST',
                    url     : 'seasonal/search/' + $scope.search_value
                }).then(function(data) {
                    $scope.kerst_products = data;
                    $scope.loading = false;
                });
            } else {
                $scope.init();
            }
        };


         /**
         * Deletes the given object by id
         * Return to the current page after delete
         *
         * @param $id : id of the object
         * @param $current_page : the page the dataset is on
         */
        $scope.delete = function($id){
            $scope.loading = true;
            $http({
                    method  : 'POST',
                    url     : 'seasonal/delete/' + $scope.path + '/' + $id
                }).then(function (success) {
                $http({
                url: 'api/seasonal/'+ $scope.path,
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function(data, status, headers, config) {
                $scope.thelastpage = data.last_page;
                $scope.data = data.data;
                $scope.currentpage = data.current_page;
                $scope.loading = false;
            });
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

    });


