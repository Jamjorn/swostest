angular.module('facturatieInvoiceCtrl', [])

// inject the Comment service into our controller
    .controller('facturatieInvoiceController', function($scope, $http, $filter, $window, $timeout, FacturatieInvoice, FacturatieCustomer, FacturatieProduct, FacturatieRoute, $q) {

        $scope.loading = true;
        $scope.once = true;
        $scope.isSaving = false;
        $scope.selected_type = '';
        $scope.selected_soort = '';

        $scope.object = {};
        $scope.product_list = [''];

        $scope.types = [{
            Id: 1,
            Name: 'los'
        }, {
            Id: 2,
            Name: 'vast'
        }];


        $scope.soorten = [{
            Id: 1,
            Name: 'Afhalen'
        }, {
            Id: 2,
            Name: 'Bezorgen'
        }];


        /**
         * Initialize the overview page
         */
        $scope.init = function() {
            FacturatieInvoice.getAllWithPagination(1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.initToevoegen = function(){
            FacturatieRoute.getAll()
                .then(function (success) {
                    $scope.routes = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.get = function($id){
            FacturatieInvoice.get($id)
                .then(function (success) {
                    $scope.object = success.data;
                    $scope.object.date = moment($scope.object.date, 'YYYY-MM-DD');
                    $scope.object.date =  $scope.object.date.locale("nl").format('DD-MM-YYYY');
                    $scope.initDropdown($scope.object.type, $scope.object.soort);
                    $scope.selected_customer = $scope.object.customer.initials + " " + $scope.object.customer.insertion + " " + $scope.object.customer.lastname;
                    $scope.selected_discount = $scope.object.customer.discount;
                    var summernote = angular.element('.note-editor .panel-body');
                    summernote.html($scope.object.description);
                    $scope.product_list = $scope.object.invoice_lines;
                    angular.forEach($scope.object.invoice_lines, function(value, key) {
                        FacturatieProduct.get(value.product_id)
                            .then(function (success) {
                                $scope.product_list[key].name = success.data.name;
                                $scope.product_list[key].price = success.data.price;
                            }).catch(function (e) {
                                console.log("got an error in the process", e);
                            });
                    });
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get data by pagination - 10 item per page
         * @param $page : the page you want the data from
         */
        $scope.getPage = function($page) {
            $scope.loading = true;
            FacturatieInvoice.getAllWithPagination($page)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get data for the next page [the next 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         * @param $last_page : is het last page of the data set
         */
        $scope.nextPage = function($current_page, $last_page) {
            if($current_page != $last_page) {
                $scope.loading = true;
                FacturatieInvoice.getAllWithPagination($current_page + 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Get data for the previous page [the previous 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         */
        $scope.previousPage = function($current_page) {
            if($current_page != 1) {
                $scope.loading = true;
                FacturatieInvoice.getAllWithPagination($current_page - 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Search for data by string.
         * If data matches part of the string one or more results will be returned
         * Else if $search_value string is empty it will return the first page of the current data set
         *
         * @param $search_value : is the given input as string
         */
        $scope.search = function($search_value) {
            if($search_value != ''){
                $scope.loading = true;
                FacturatieInvoice.search($search_value)
                    .then(function (success) {
                        $scope.data = success;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                FacturatieInvoice.getAllWithPagination(1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Deletes the given object by id
         * Return to the current page after delete
         *
         * @param $id : id of the object
         * @param $current_page : the page the dataset is on
         */
        $scope.delete = function($id){
            $scope.loading = true;
            FacturatieInvoice.delete($id)
                .then(function (success) {
                    FacturatieInvoice.getAllWithPagination(success.current_page)
                        .then(function (success) {
                            $scope.data = success.data;
                        }).catch(function (e) {
                            console.log("got an error in the process", e);
                        });
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.addObject = function(){
            $scope.isSaving = true;
            $scope.validationMessage = false;

            $scope.object.type = $scope.selected_type.Name;
            $scope.object.soort = $scope.selected_soort.Name;
            $scope.object.product_list = $scope.product_list;
            var summernote = angular.element('.note-editor .panel-body').html().trim();
            if(summernote == '<p><br></p>'){ summernote = ''; }
            $scope.object.description = summernote;

            FacturatieInvoice.addObject($scope.object)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een bestelling aangemaakt.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        $scope.editObject = function($id){
            $scope.isSaving = true;
            $scope.validationMessage = false;

            $scope.object.type = $scope.selected_type.Name;
            $scope.object.soort = $scope.selected_soort.Name;
            $scope.object.product_list = $scope.product_list;
            var summernote = angular.element('.note-editor .panel-body').html().trim();
            if(summernote == '<p><br></p>'){ summernote = ''; }
            $scope.object.description = summernote;
            FacturatieInvoice.editObject($scope.object, $id)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een bestelling gewijzigd.';
                    $window.location.href = '/facturatie/invoice/edit/'+(parseFloat($id, 6) +1);
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        $scope.searchCustomers = function($search_value){
            if($search_value != ''){
                $scope.loading = true;
                FacturatieCustomer.search($search_value)
                    .then(function (success) {
                        $scope.customers = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                $scope.customers = null;
                $scope.loading = false;
            }
        };

        $scope.searchProducts = function($search_value){
            if($search_value != ''){
                $scope.loading = true;
                FacturatieProduct.search($search_value)
                    .then(function (success) {
                        $scope.products = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                $scope.products = null;
                $scope.loading = false;
            }
        };

        $scope.selectCustomer = function($customer){
            $scope.selected_customer = $customer.initials + " " + $customer.insertion + " " + $customer.lastname;
            $scope.selected_discount = $customer.discount;
            $scope.object.customer_id = $customer.id;
            angular.element(document.querySelector('#customerModal .close')).click();
        };

        $scope.selectProduct = function($product, $index){
            $scope.selected_product = $product.name;
            $scope.object.product_id = $product.id;
            $scope.product = '';
            $scope.products = null;
            $scope.addProduct($product, $index);
            angular.element(document.querySelector('#productModal'+$index+' .close')).click();
        };

        $scope.selectRoute = function($route){
            $scope.selected_route = $route.route_number;
            $scope.selected_route_price = $route.price;
            $scope.object.route_id = $route.id;
            $scope.changeQuantity();
            angular.element(document.querySelector('#routeModal .close')).click();
        };

        $scope.addProduct = function($product, $index){
            $scope.product_list[$index] = $product;
        };

        $scope.extendProductList = function(){
            $scope.product_list.push('');
        };

        $scope.changeQuantity = function(){

            var totalprice = 0;

            if($scope.selected_discount) {
                var discount = parseFloat($scope.selected_discount, 6);
            } else {
                discount = 0;
            }

            if($scope.selected_route_price && $scope.selected_soort.Name != "Afhalen") {
                var route_price = parseFloat($scope.selected_route_price, 6);
            } else {
                route_price = 0;
            }

            if($scope.product_list.length > 0 && $scope.product_list[0] != "") {
                angular.forEach($scope.product_list, function (value, key) {
                    var price = parseFloat(value.price, 6);
                    var quantity = parseFloat(value.quantity, 6);
                    totalprice = totalprice + (price * quantity);
                });
                totalprice = (totalprice + route_price);
                totalprice = totalprice - ((discount / 100) * totalprice);
            } else {
                totalprice = totalprice + route_price;
                totalprice = totalprice - ((discount / 100) * totalprice);
            }
            $scope.object.price = $filter('number')(totalprice, 2);
        };

        $scope.searchRoutes = function($search_value){
            if($search_value != ''){
                $scope.loading = true;
                FacturatieRoute.search($search_value)
                    .then(function (success) {
                        $scope.routes = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                $scope.routes = null;
                $scope.loading = false;
            }
        };

        $scope.GetValueTypeDropdown = function () {
            var typeID = this.selected_type.Id;
            $scope.selected_type = $.grep($scope.types, function (selected_type) {
                return selected_type.Id == typeID;
            })[0];
            return $scope.selected_type;
        };

        $scope.GetValueSoortDropdown = function () {
            var soortID = this.selected_soort.Id;
            $scope.selected_soort = $.grep($scope.soorten, function (selected_soort) {
                return selected_soort.Id == soortID;
            })[0];
            $scope.changeQuantity();
            return $scope.selected_soort;
        };

        $scope.initDropdown = function($type, $soort){
            if($type) {
                if ($type == 'los') {
                    $scope.selected_type = $scope.types[0];
                } else if ($type == 'vast') {
                    $scope.selected_type = $scope.types[1];
                }
            }
            if($soort){
                if ($soort == 'Afhalen') {
                    $scope.selected_soort = $scope.soorten[0];
                } else if ($soort == 'Bezorgen') {
                    $scope.selected_soort = $scope.soorten[1];
                }
            }
        };

        $scope.$watch('object.date', function(newVal, oldVal) {
            if(newVal) {
                var date = moment(newVal, 'DD-MM-YYYY');
                $scope.object.day = date.locale("nl").format('dddd');
            }
        });

        /**
         * Convert image
         * to a base64 data uri
         * @param  {String}   url
         * @param  {Function} callback
         * @param  {String}   [outputFormat=image/png]
         */
        function convertImageToDataURI(url, callback, outputFormat) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function(){
                var canvas = document.createElement('CANVAS'),
                    ctx = canvas.getContext('2d'), dataURL;
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL);
                canvas = null;
            };
            img.src = url;
        }

        $scope.logo = null;
        convertImageToDataURI('/img/bakkerij-van-der-westen.png', function(base64Decoded){
            $scope.logo = base64Decoded;
        });

var dd  = {};
var set = 'no';

function contains(arr, element) {
for(var i = 0; i < arr.length; i++) {
    if (arr[i].btw == element) {
        return true;
        break;
    }
}
return false;
}

$scope.openPdf = function(id) {
  var factuurInf = [];
  var body = [];
  var summary = [];
  var count = 0;
  var maxCount = 0;
  var done = false;
  var totalFromtotal = 0;
  var btwArray = [];
  var btwObjects= [];
  var summaryTotal= [];
  var totaalInclBtw = 0;
  var docDefinition;
  var count =0;
  var win = window.open('', '_blank');

   FacturatieInvoice.get(id)
       .then(function (success) {

            $scope.pdf = success.data;
            $scope.loading = false;
            var factuurInfo = new Array();

            var strDate = $scope.pdf.date;
            var date = new Date($scope.pdf.date);
            var dueDate = new Date($scope.pdf.date);

            var starTday = date.getDate();
            var starTmonth = date.getMonth()+1;
            var starTyear = date.getFullYear();

            dueDate.setDate(dueDate.getDate() + 14);

            var day = dueDate.getDate();
            var month = dueDate.getMonth()+1;
            var year = dueDate.getFullYear();

            factuurInfo.push( starTday+" "+starTmonth+''+starTyear+" "+$scope.pdf.id);
            factuurInfo.push( $scope.pdf.customer_id);
            factuurInfo.push( 'NL 123456789 B01' );

            factuurInfo.push(starTday +'-'+ starTmonth +'-'+ starTyear);
            factuurInfo.push( day +'-'+ month +'-'+ year );
            factuurInf.push(factuurInfo);

            angular.forEach($scope.pdf.invoice_lines, function(value, key) {
                maxCount++
            });

            $scope.product_list = $scope.pdf.invoice_lines;
            angular.forEach($scope.pdf.invoice_lines, function(value, key) {
                FacturatieProduct.get(value.product_id)
                    .then(function (success) {
                        $scope.product_list[key].name = success.data.name;
                        $scope.product_list[key].price = success.data.price;
                        $scope.product_list[key].btw = success.data.btw;


                        var rowTotal = $scope.product_list[key].price*$scope.product_list[key].quantity;
                        totalFromtotal = totalFromtotal + rowTotal;
                        var fila = new Array();
                        fila.push( $scope.product_list[key].name + " ");
                        fila.push( $scope.product_list[key].quantity+ " ");
                        fila.push( $scope.product_list[key].btw+'%'+ " ");
                        fila.push( $scope.product_list[key].price+ " ");
                        fila.push( rowTotal.toFixed(2)+ " ");

                        body.push(fila);

                        ////
                        if(btwArray.length == 0 || !contains(btwObjects, $scope.product_list[key].btw ) ){
                            btwArray.push($scope.product_list[key].btw);
                            var btw = {price:$scope.product_list[key].price, btw:$scope.product_list[key].btw};
                            btwObjects.push(btw);


                        }
                        else{
                            for(var i = 0; i < btwObjects.length; i++) {
                                if (btwObjects[i].btw == $scope.product_list[key].btw) {
                                    btwObjects[i].price = parseInt(btwObjects[i].price) + parseInt($scope.product_list[key].price);
                                break;

                                }

                            }
                        }

                        ///


                        count++
                        if(count == maxCount){
                            done = true
                        }

                        if(done === true){

                        var sum = new Array();
                        sum.push(" ");
                        sum.push(" ");
                        sum.push(" ");;
                        if($scope.pdf.customer.discount > 0){
                            sum.push('Korting('+$scope.pdf.customer.discount +'%'+')');
                        }
                        else{
                            sum.push('');
                        }

                        var discount = totalFromtotal/100 * $scope.pdf.customer.discount;
                        if($scope.pdf.customer.discount > 0){
                                    sum.push('\u20AC' + ' ' +'-' + ' ' + discount.toFixed(2));
                            }
                        else{
                            sum.push(" ");
                        }

                        summary.push(sum);

                        var sum = new Array();
                        sum.push(" ");
                        sum.push(" ");
                        sum.push(" ");
                        sum.push('Totaal excl. btw');

                        sum.push('\u20AC' + ' '+ ' '  + ' ' + totalFromtotal.toFixed(2));
                        summary.push(sum);



                        for(var i = 0; i < btwObjects.length; i++) {
                        btwObjects[i].btw
                        var sum = new Array();
                        sum.push(" ");
                        sum.push(" ");
                        sum.push(" ");
                        sum.push(btwObjects[i].btw +'%'  + ' ' + 'btw over' + ' ' + btwObjects[i].price);
                        var totaalBtw = btwObjects[i].price/100 * btwObjects[i].btw;
                        sum.push('\u20AC' + ' '+ ' '  + ' ' + totaalBtw.toFixed(2));
                        summary.push(sum);
                        totaalInclBtw = totaalInclBtw + totaalBtw;
                        }

                         var sum = new Array();
                        sum.push(" ");
                        sum.push(" ");
                        sum.push(" ");
                        sum.push({text: 'Totaal Inclusief. btw', style: 'Total'});
                        var stepOne = totalFromtotal - discount;
                        var stepTwo = stepOne + totaalInclBtw;
                        sum.push({border: [true, true, true, true], text:'\u20AC' + ' '+ ' '  + ' ' + stepTwo.toFixed(2), style: 'Total'});
                        summaryTotal.push(sum);

                docDefinition = {
                      // a string or { width: number, height: number }


// [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins
//  pageMargins: [ 20, 20, 20, 20 ],

footer: function(currentPage, pageCount) {
    return {
        margin: [20, 15],
        fontSize: 8,
        alignment: 'right',
        text: 'Page ' + currentPage.toString() + ' of ' + pageCount
    };
},

                       content: [
/////////////////////////////////////////////////////////////////////Header///////////////////////////////////////////////////////////////////////////////////////////////////

                       {
                           table: {
                                   widths: ['auto', '*'],
                                   body:[
                                           [
                                               {
                                                   alignment: 'left',                                                                                                                        image: $scope.logo,
                                                   width: 160

                                                },
                                                {
                                               width: '*',
                                               alignment: 'right',
                                               stack: [
                                                            {
                                                               style: 'small',
                                                               text: ' '
                                                           },
                                                           {
                                                               style: 'small',
                                                               text: 'Van der Westen:'
                                                           },
                                                           {
                                                               style: 'small',
                                                               text: 'StraatNaam'
                                                           },
                                                           {
                                                               style: 'small',
                                                               text: 'Postcode en plaatsnaam'
                                                           }
                                                           ,
                                                           {
                                                               style: 'small',
                                                               text: " "
                                                           }
                                                           ,
                                                           {
                                                               style: 'small',
                                                               text: 'Email: email@email.com'
                                                           }
                                                           ,
                                                           {
                                                               style: 'small',
                                                               text: 'Bank: NL19875613280085'
                                                           }
                                                           ,
                                                           {
                                                               style: 'small',
                                                               text: 'Banknummer: NL44545454445'
                                                           },
                                                           {
                                                               style: 'small',
                                                               text: 'Kvknummer: 55555555555'
                                                           }
                                                       ]
                                                }

                                           ]
                                       ]
                                   },
                                   layout: {
                                       hLineWidth: function(line) {  },
                                       vLineWidth: function() { return 0; },
                                       paddingBottom: function() { return 35; }
                                   }
                       },
///////////////////////////////////////////////////////////////////////Factuur gegevens///////////////////////////////////////////////////////////////////////////////////////
  {
                           table: {
                                   widths: ['auto', '*'],
                                   body:[
                                           [
                                               {
                                               width: '*',
                                               alignment: 'left',
                                               stack: [
                                                            {
                                                               style: 'header',
                                                               text: 'Factuur'
                                                           },

                                                           {   style: 'h2',
                                                               text: " "
                                                           },

                                                           {   style: 'bigger',
                                                               text: $scope.pdf.customer.initials + ' ' + $scope.pdf.customer.insertion + ' ' + $scope.pdf.customer.lastname
                                                           },
                                                           {
                                                               style: 'bigger',
                                                               text: $scope.pdf.customer.address+ ' ' + $scope.pdf.customer.housenumber,
                                                           },
                                                           {
                                                               style: 'bigger',
                                                               text: $scope.pdf.customer.zipcode+ ' ' + $scope.pdf.customer.town
                                                           }
                                                       ]
                                                   },
                                                   {
                                                   },
                                           ]
                                       ]
                                   },
                                   layout: {
                                       hLineWidth: function(line) {  },
                                       vLineWidth: function() { return 0; },
                                       paddingBottom: function() { return 50; }
                                   }
                       },

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      {
    table: {

        widths: [ '*', '*', '*', '*', '*' ],
        body: [
            [
            {border: [false, false, false, true],
             text: 'FactuurNummer', style: 'tableHeader'
            },
            {border: [false, false, false, true],
                text: 'Klantnummer', style: 'tableHeader'},
            {border: [false, false, false, true],
                text: 'Btw-nummer', style: 'tableHeader'},
            {border: [false, false, false, true],
                text: 'Factuurdatum', style: 'tableHeader'},
            {border: [false, false, false, true],
                text: 'Vervaldatum', style: 'tableHeader'}
            ],
        ]
    },layout: {
                                       hLineWidth: function(line) { return 2.5; },
                                       vLineWidth: function() { return 0; },
                                       paddingBottom: function() { return 5; },
                                        hLineColor: function (i, node) {
                                            return (i === 0 || i === node.table.body.length) ? '#f6b11a' : '#f6b11a';
                                        }
                                   }
},
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   {
                           table: {
                                    widths: [ 95,95,95,95,100 ],
                                    body: factuurInf
                                   },
                                   layout: {
                                       hLineWidth: function(line) { return 0; },
                                       vLineWidth: function() { return 0; },
                                       paddingBottom: function() { return 20; },
                                       paddingTop: function() { return 5; }

                                   }
                       },
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      {
    table: {

        widths: [ '*', '*', '*', '*', '*' ],
        body: [
            [
            {border: [false, false, false, true],
             text: 'Omschrijving', style: 'tableHeader'
            },
            {border: [false, false, false, true],
                text: 'Aantal', style: 'tableHeader'},
            {border: [false, false, false, true],
                text: 'Btw', style: 'tableHeader'},
            {border: [false, false, false, true],
                text: 'Prijs', style: 'tableHeader'},
            {border: [false, false, false, true],
                text: 'Totaal', style: 'tableHeader'}
            ],
        ]
    },layout: {
                                       hLineWidth: function(line) { return 2.5; },
                                       vLineWidth: function() { return 0; },
                                       paddingBottom: function() { return 5; },
                                        hLineColor: function (i, node) {
                                            return (i === 0 || i === node.table.body.length) ? '#f6b11a' : '#f6b11a';
                                        }
                                   }
},
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   {
                           table: {
                                    widths: [ 95,95,95,95,100 ],
                                    body: body
                                   },
                                   layout: {
                                       hLineWidth: function(line) { return 0; },
                                       vLineWidth: function() { return 0; },
                                       paddingBottom: function() { return 5; },
                                       paddingTop: function() { return 5; }
                                   }
                       },
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  {
   text: " ",
            margin: [0, 0,20,0],
 },
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   {
                           table: {
                                    widths: [ 95,95,90,100,100 ],

                            body: summary

                                   },
                                   layout: {
                                       hLineWidth: function(line) { return 0; },
                                       vLineWidth: function() { return 0; },
                                       paddingBottom: function() { return 0; },
                                       paddingTop: function() { return 5; }
                                   }
                       },
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            {
   text: " ",
            margin: [0, 0,20,0],
 },
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             {
                           table: {
                                    widths: [ 95,95,90,100,100 ],

                            body: summaryTotal

                                   },
                                   layout: {
                                       paddingBottom: function() { return 2; },
                                       paddingTop: function() { return 2; },
                                        defaultBorder: false,
                                        hLineWidth: function (i, node) {
                                            return (i === 0 || i === node.table.body.length) ? 1 : 1;
                                        },
                                        vLineWidth: function (i, node) {
                                            return (i === 0 || i === node.table.widths.length) ? 1 : 1;
                                        },
                                        hLineColor: function (i, node) {
                                            return (i === 0 || i === node.table.body.length) ? '#f6b11a' : '#f6b11a';
                                        },
                                        vLineColor: function (i, node) {
                                            return (i === 0 || i === node.table.widths.length) ?  '#f6b11a' : '#f6b11a';
                                        },
                       }
                   },
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      {
    table: {

        widths: [ '*', '*', '*', '*', '*' ],
        body: [
            [
            {border: [false, false, false, true],
             text: " ",
            },
            {border: [false, false, false, true],
                text: " ",},
            {border: [false, false, false, true],
                text: " ",},
            {border: [false, false, false, true],
                text: " ", },
            {border: [false, false, false, true],
                text: " ",}
            ],
        ]
    },layout: {
                                       hLineWidth: function(line) { return 2.5; },
                                       vLineWidth: function() { return 0; },
                                       paddingTop: function() { return 15; },
                                        hLineColor: function (i, node) {
                                            return (i === 0 || i === node.table.body.length) ? '#f6b11a' : '#f6b11a';
                                        }
                                   }
},
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ],


  styles: {
   header: {
       fontSize: 15,
       bold: true
   },
   Total: {
       fontSize: 11,
       bold: true,
   },
   tableHeader:{
    fontSize:11,
   },
   bigger: {
       fontSize: 10,
   },
   small: {
   fontSize: 8
}
 },
defaultStyle: {
   columnGap: 20,
   fontSize: 10,
   alignment: 'right'
}
}

                pdfMake.createPdf(docDefinition).open({}, win);
};

                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });



            });


            }).catch(function (e) {
                console.log("got an error in the process", e);
            });

};



        $scope.$watch('selected_soort', function(newVal, oldVal) {
            if(newVal.Name == 'Afhalen'){
                $scope.bezorgen = false;
                $scope.selected_route = 0;
                $scope.selected_route_price = 0.00;
                $scope.object.route_id = 0;
            } else if(newVal.Name == 'Bezorgen') {
                $scope.bezorgen = true;
                $scope.selected_route = '';
                $scope.object.route_id = '';
                if($scope.object.route_number != ""){
                    $scope.selected_route = $scope.object.route.route_number;
                    $scope.object.route_id = $scope.object.route.id;
                }
            }
        });


    });

