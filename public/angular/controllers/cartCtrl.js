angular.module('cartCtrl', ['ngStorage'])

    .controller('cartController', function($scope, $http, $timeout, Cart) {

        var loading = true;
        $scope.productCounter = 0;
        $scope.cart = [];
        $scope.currentQuantity = [];

        $scope.ophalen = false;
        $scope.bezorgen = false;

        var clickMe = $('.clickMe');
        var mijnLijst = $('.targetMe');
        var fromtop = mijnLijst.offset().top - $(window).scrollTop(); // top of screen to mijn lijst btn
        var fromright = 290 + (((($(window).width() / 12) * 9) / 12) * 3); // to +- first product item
        clickMe.css('top', fromtop);
        clickMe.css('left', fromright);

        $scope.init = function(){
            $scope.initProductCounter();
            Cart.getCart()
                .then(function (success){
                    $scope.cart = success.data;
                }).catch(function (e){
                    console.log("got an error in the process", e);
                });
        }

        $scope.addToCart = function ($product, $quantity, $half_bread) {

            addAnimation();

            if($half_bread){
                $product.half_bread = true;
            } else {
                $product.half_bread = false;
            }

            $product.quantity = $quantity;

            Cart.addToCart($product)
                .then(function (success){
                    if($half_bread){
                        $scope.currentQuantity["b" + $product.id] =  $scope.currentQuantity["b" + $product.id] + $quantity;
                    } else {
                        $scope.currentQuantity[$product.id] =  $scope.currentQuantity[$product.id] + $quantity;
                    }
                }).catch(function (e){
                    console.log("got an error in the process", e);
                });
            // totalprice: parseFloat(product.price_half_bread, 6),
        }

        $scope.changeQuantity = function($product, $quantity, $half_bread){

            $product.quantity = parseFloat($quantity, 6);
            $product.totalprice = parseFloat($product.price * $product.quantity, 6);

            addAnimation();

            Cart.changeQuantity($product)
                .then(function (success){
                    if($half_bread){
                        $scope.currentQuantity["b" + $product.id] =  $scope.currentQuantity["b" + $product.id] + $quantity;
                    } else {
                        $scope.currentQuantity[$product.id] = $product.quantity;
                    }
                }).catch(function (e){
                    console.log("got an error in the process", e);
                });

            //if (quantity < 9999 && quantity >= 0 && quantity != '') {
            //
            //    if (type == 'standaard') {
            //        var id = product.id;
            //        quantity = parseFloat(quantity, 6);
            //
            //        var found = false;
            //        $scope.cart.forEach(function (item) {
            //            if (item.id === product.id) {
            //
            //                if (quantity == 0) {
            //                    $scope.productCounter = $scope.productCounter - item.quantity;
            //                    $scope.cart.splice($scope.cart.indexOf(item), 1);
            //                }
            //                else if (item.quantity < quantity) {
            //                    $scope.productCounter = $scope.productCounter + (quantity - item.quantity);
            //                }
            //                else if (item.quantity > quantity) {
            //                    $scope.productCounter = $scope.productCounter - (item.quantity - quantity);
            //                }
            //
            //                item.quantity = quantity;
            //                item.totalprice = parseFloat(item.price * quantity, 6);
            //
            //                $scope.currentQuantity[id] = item.quantity;
            //                found = true;
            //            }
            //        });
            //        if (!found) {
            //            $scope.cart.push(angular.extend({
            //                    quantity: quantity,
            //                    totalprice: parseFloat(product.price * quantity, 6)
            //                },
            //                product));
            //            $scope.productCounter += quantity;
            //        }
            //    } else if (type == 'brood') {
            //
            //        var id = "b" + product.id;
            //        quantity = parseFloat(quantity, 6);
            //        var found = false;
            //
            //        $scope.cart.forEach(function (item) {
            //            if (angular.isNumber(product.id)) {
            //                if (item.id === "b" + product.id) {
            //                    if (quantity == 0) {
            //                        $scope.productCounter = $scope.productCounter - item.quantity;
            //                        $scope.cart.splice($scope.cart.indexOf(item), 1);
            //                    }
            //                    else if (item.quantity < quantity) {
            //                        $scope.productCounter = $scope.productCounter + (quantity - item.quantity);
            //                    }
            //                    else if (item.quantity > quantity) {
            //                        $scope.productCounter = $scope.productCounter - (item.quantity - quantity);
            //                    }
            //
            //                    item.quantity = quantity;
            //                    item.totalprice = parseFloat(item.price * quantity, 6);
            //
            //                    $scope.currentQuantity[id] = item.quantity;
            //                    found = true;
            //                }
            //            } else if (angular.isString(product.id)) {
            //                if (item.id === product.id) {
            //                    if (quantity == 0) {
            //                        $scope.productCounter = $scope.productCounter - item.quantity;
            //                        $scope.cart.splice($scope.cart.indexOf(item), 1);
            //                    }
            //                    else if (item.quantity < quantity) {
            //                        $scope.productCounter = $scope.productCounter + (quantity - item.quantity);
            //                    }
            //                    else if (item.quantity > quantity) {
            //                        $scope.productCounter = $scope.productCounter - (item.quantity - quantity);
            //                    }
            //
            //                    item.quantity = quantity;
            //                    item.totalprice = parseFloat(item.price * quantity, 6);
            //
            //                    $scope.currentQuantity[id] = item.quantity;
            //                    found = true;
            //                }
            //            }
            //        });
            //        if (!found) {
            //            $scope.cart.push({
            //                id: id,
            //                name: product.name + ' (half)',
            //                type: product.type,
            //                categories: product.categories,
            //                price: product.price_half_bread,
            //                path: product.path,
            //                details: product.details,
            //                rating: product.rating,
            //                availability: product.availability,
            //                warning: product.warning,
            //                postnl: product.postnl,
            //                quantity: quantity,
            //                totalprice: parseFloat(product.price_half_bread * quantity, 6),
            //                brood: true
            //            });
            //            $scope.productCounter += quantity;
            //        }
            //
            //    }


            }


        $scope.removeSingleItemFromCart = function ($product, $quantity, $half_bread) {


            if($half_bread){
                $product.half_bread = true;
            } else {
                $product.half_bread = false;
            }

            $product.quantity = $quantity;

            Cart.removeSingleItemFromCart($product)
                .then(function (success){
                    if($half_bread){
                        $scope.currentQuantity["b" + $product.id] =  $scope.currentQuantity["b" + $product.id] - $quantity;
                    } else {
                        $scope.currentQuantity[$product.id] =  $scope.currentQuantity[$product.id] - $quantity;
                    }
                }).catch(function (e){
                    console.log("got an error in the process", e);
                });
        }



        $scope.deleteCakeFromCart = function($index)
        {
            $scope.productCounter--;
            $scope.cart.splice($index,1);

            this.saveProducts($scope.cart);
            this.saveProductCounter($scope.productCounter);
        };

        $scope.addCakeToCart = function (product, $id, $selected_vorm, $selected_personen, $selected_tekst_vraag, $selected_tekst,
                                         $selected_gesneden, $selected_buitenkant, $selected_vulling, $prijs_for_taart
        ) {

            if($selected_vorm != '') {
                if ($selected_personen != '') {
                    if ($selected_tekst_vraag != '') {
                        if ($selected_gesneden != '') {
                            if ($selected_buitenkant != '') {
                                if ($selected_vulling != '') {
                                    if ($prijs_for_taart != '') {

                                        $("body,html").css("overflow-y", "scroll");
                                        $(".overlay-webshop").css("display", "none");
                                        $(".taart-samenstellen-popover").css("display", "none");

                                        $scope.cart.push({
                                            id: $id,
                                            name: product.name,
                                            type: product.type,
                                            categories: product.categories,
                                            path: product.path,
                                            details: product.details,
                                            rating: product.rating,
                                            availability: product.availability,
                                            warning: product.warning,
                                            postnl: product.postnl,
                                            quantity: 1,
                                            shape: $selected_vorm,
                                            people: $selected_personen,
                                            text_option: $selected_tekst_vraag,
                                            text: $selected_tekst,
                                            gesneden: $selected_gesneden,
                                            buitenkant: $selected_buitenkant,
                                            vulling: $selected_vulling,
                                            price: $prijs_for_taart
                                        });
                                        $scope.productCounter += 1

                                        $scope.amount_to_cart = 1;
                                        moveMe();

                                        $scope.saveProducts($scope.cart);
                                        $scope.saveProductCounter($scope.productCounter);
                                    } else {
                                        $scope.taart_error = 'Uw prijs is nog niet samengesteld.';
                                    }
                                } else{
                                    $scope.taart_error = 'U moet nog een vulling selecteren.';
                                }
                            } else {
                                $scope.taart_error = 'U moet nog een buitenkant selecteren.';
                            }
                        } else {
                            $scope.taart_error = 'U moet nog een gesneden optie selecteren.';
                        }
                    } else {
                        $scope.taart_error = 'U moet nog een antwoord geven op tekst op taart.';
                    }
                } else {
                    $scope.taart_error = 'U moet nog het aantal personen selecteren.';
                }
            } else{
                $scope.taart_error = 'U moet nog een vorm selecteren.';
            }
        };


        $scope.removeFromCart = function (product, type) {

            if (type == "standaard") {
                var id = product.id;

                $scope.cart.forEach(function (item) {
                    if (item.id === product.id) {
                        if (item.quantity != 0) {
                            item.quantity--;
                            $scope.productCounter--;
                            item.totalprice -= parseFloat(item.price, 6);
                            $scope.currentQuantity[id] = item.quantity;

                            if (item.quantity == 0) {
                                $scope.cart.splice($scope.cart.indexOf(item), 1);
                            }
                        }
                    }

                });
            } else if (type == "brood") {

                var id = "b" + product.id;

                $scope.cart.forEach(function (item) {
                    if (angular.isNumber(product.id)) {
                        if (item.id === "b" + product.id) {
                            if (item.quantity != 0) {
                                item.quantity--;
                                $scope.productCounter--;
                                item.totalprice -= parseFloat(item.price, 6);
                                $scope.currentQuantity[id] = item.quantity;

                                if (item.quantity == 0) {
                                    $scope.cart.splice($scope.cart.indexOf(item), 1);
                                }
                            }
                        }
                    } else if (angular.isString(product.id)) {
                        if (item.id === product.id) {
                            if (item.quantity != 0) {
                                item.quantity--;
                                $scope.productCounter--;
                                item.totalprice -= parseFloat(item.price, 6);
                                $scope.currentQuantity[id] = item.quantity;

                                if (item.quantity == 0) {
                                    $scope.cart.splice($scope.cart.indexOf(item), 1);
                                }
                            }
                        }
                    }
                });
            }

            this.saveProducts($scope.cart);
            this.saveProductCounter($scope.productCounter);

        };

        $scope.deleteFromCart = function (product, type) {

            if (type == 'standaard') {
                $scope.cart.forEach(function (item) {
                    if (item.id === product.id) {
                        $scope.productCounter = $scope.productCounter - item.quantity;
                        $scope.cart.splice($scope.cart.indexOf(item), 1);
                    }
                });
            } else if (type == 'brood') {
                $scope.cart.forEach(function (item) {
                    if (angular.isNumber(product.id)) {
                        if (item.id === "b" + product.id) {
                            $scope.productCounter = $scope.productCounter - item.quantity;
                            $scope.cart.splice($scope.cart.indexOf(item), 1);
                        }
                    } else if (angular.isString(product.id)) {
                        if (item.id === product.id) {
                            $scope.productCounter = $scope.productCounter - item.quantity;
                            $scope.cart.splice($scope.cart.indexOf(item), 1);
                        }
                    }
                });
            }

            this.saveProducts($scope.cart);
            this.saveProductCounter($scope.productCounter);

        };


        $scope.getCartPrice = function () {
            $scope.total = 0;
            $scope.cart.forEach(function (product) {
                $scope.total += product.price * product.quantity;
            });

            return $scope.total;
        };

        $scope.getEventualCartPrice = function () {
            $scope.total = 0;
            $scope.cart.forEach(function (product) {
                $scope.total += product.price * product.quantity;
            });

            if ($scope.total < 10) {
                $scope.total = $scope.total + 2.50;
            }

            return $scope.total;
        };

        $scope.getSingeProductCarCount = function (productID) {

            var quantity = 0;

            $scope.cart.forEach(function (item) {
                if (item.id === productID) {
                    quantity = item.quantity;
                }
            });

            return quantity;
        };

        $scope.checkId = function (product) {

            $scope.cart.forEach(function (item) {
                if (item.id !== product.id) {
                    $scope.checkid = true;
                    return $scope.checkid;
                } else {
                    $scope.checkid = false;
                    return $scope.checkid;
                }
            });

        };

        $scope.getCurrentQuantity = function (product, type) {

            if (type == 'standaard') {
                var id = product.id;
                var found = false;

                $scope.cart.forEach(function (item) {

                    if (item.id === product.id) {
                        $scope.currentQuantity[id] = item.quantity;
                        found = true;
                        return item.quantity;
                    }

                });
                if (found == false) {
                    $scope.currentQuantity[id] = 0;
                    return 0;
                }
            } else if (type == 'brood') {

                var id = "b" + product.id;
                var found = false;

                $scope.cart.forEach(function (item) {

                    if (angular.isNumber(product.id)) {
                        if (item.id === "b" + product.id) {
                            $scope.currentQuantity[id] = item.quantity;
                            found = true;
                            return item.quantity;
                        }
                    } else if (angular.isString(product.id)) {
                        if (item.id === product.id) {
                            $scope.currentQuantity[id] = item.quantity;
                            found = true;
                            return item.quantity;
                        }
                    }

                });
                if (found == false) {
                    $scope.currentQuantity[id] = 0;
                    return 0;
                }

            }

        };

        $scope.clearStorage = function () {
            $sessionStorage.$reset();
        };

        $scope.initProductCounter = function(){
            if ($sessionStorage.productCounter != "" && $sessionStorage.productCounter !== undefined) {
                $scope.productCounter = $sessionStorage.productCounter;
            } else {
                $scope.productCounter = 0;
            }
        }

        function addAnimation(target) {
            if (loading == true) {

                loading = false;
                clickMe.css('display', 'block');

                var target = $('.targetMe');
                var left = target.offset().left - clickMe.offset().left;
                var left_offset = clickMe.offset().left;

                clickMe.css({transform: 'translate(' + (left + 50) + 'px)', opacity: 0});

                $scope.animate = true;

                setTimeout(function () {
                    clickMe.css({transform: 'translate(0px)', opacity: 1, 'display': 'none'});
                    loading = true;
                }, 2500);
            }
        }

    });



