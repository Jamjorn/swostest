angular.module('facturatieRouteManagerCtrl', [])

// inject the Comment service into our controller
    .controller('facturatieRouteManagerController', function($scope, $http, $filter, $timeout, FacturatieRouteManager, FacturatieInvoice, FacturatieProduct, FacturatieRoute) {

        $scope.loading = true;
        $scope.once = true;
        $scope.isSaving = false;

        $scope.selected_type = '';
        $scope.my_index = 0;
        $scope.view = 'benodigdheden';

        $scope.product_list = [];
        $scope.object = {};

        $scope.generateRoutes = function($week_nr, $year){
            FacturatieRouteManager.generateRoutes($week_nr, $year)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol de routes voor week: ' + $week_nr + ' gegenereerd.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        $scope.loading = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                    $scope.loading = false;
                });
        };

        $scope.checkRoutesAreGenerated = function($week_nr, $year){
            FacturatieRouteManager.checkRoutesAreGenerated($week_nr, $year)
                .then(function (success){
                    $scope.routesAreGenerated = success.data;
                    $scope.loading = false;
                }).catch(function (errors){
                    $scope.loading = false;
                });
        };

        // @TODO remove first 3 variables if possible
        $scope.loadRouteData = function($day, $week_nr, $year){
            $scope.day = $day;
            $scope.week_nr = $week_nr;
            $scope.year = $year;

            FacturatieRouteManager.loadRouteData($day, $week_nr, $year)
                .then(function (success){
                    $scope.data = success.data;
                    FacturatieRoute.get($scope.data[0].route_id)
                        .then(function (success) {
                            $scope.route = success.data;
                            $scope.loading = false;
                        }).catch(function (e) {
                            console.log("got an error in the process", e);
                        });
                    $scope.initBezorgen();
                    $scope.initBenodigdheden();
                    $scope.loading = false;
                }).catch(function (errors){
                    $scope.loading = false;
                });
        };

        $scope.calculateDistance = function($current_location_latitude, $current_location_longitude, $latitude, $longitude){
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad($latitude-$current_location_latitude);  // deg2rad below
            var dLon = deg2rad($longitude-$current_location_longitude);
            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    Math.cos(deg2rad($current_location_latitude)) * Math.cos(deg2rad($latitude)) *
                    Math.sin(dLon/2) * Math.sin(dLon/2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c; // Distance in km
            return d;
        };

        function deg2rad(deg) {
            return deg * (Math.PI/180)
        }

        $scope.initBezorgen = function(){
            $scope.current_location_latitude = 0; // @TODO : Replace by location R'veer, Rsdnk, Bakkerij
            $scope.current_location_longitude = 0;  // @TODO : Replace by location R'veer, Rsdnk, Bakkerij

            angular.forEach($scope.data, function(value, key) {
                $scope.data[key].distance = $scope.calculateDistance($scope.current_location_latitude,
                                                                     $scope.current_location_longitude,
                                                                     value.customer.latitude,
                                                                     value.customer.longitude);
            });
            $scope.data = $filter('orderBy')($scope.data, '+distance');
            console.log($scope.data);
        };

        $scope.initBenodigdheden = function(){
            angular.forEach($scope.data, function(value, key) {
                angular.forEach($scope.data[key].invoice_lines, function(value, key) {
                    var totalprice = parseFloat(parseFloat(value.product.price,6) * parseFloat(value.quantity), 6);
                    var product = {name: value.product.name,
                                   price: value.product.price,
                                   quantity: value.quantity,
                                   totalprice: $filter('number')(totalprice, 2)};
                    if($scope.product_list.length > 0) {
                        angular.forEach($scope.product_list, function (value, key) {
                            if (value.name == product.name) {
                                totalprice = parseFloat(parseFloat(value.totalprice) + parseFloat(product.totalprice), 6);
                                $scope.product_list[key] = {
                                    name: product.name,
                                    price: product.price,
                                    quantity: product.quantity + value.quantity,
                                    totalprice: $filter('number')(totalprice, 2)
                                };
                            } else {
                                $scope.product_list.push(product);
                            }
                        });
                    } else {
                        $scope.product_list.push(product);
                    }
                });
            });
            console.log($scope.product_list);
        };

        $scope.initGenerate = function($week_nr, $year, $all){
            FacturatieRouteManager.checkRoutesAreGenerated($week_nr, $year, $all)
                .then(function (success){
                    $scope.generation_list = success.data;
                    $scope.loading = false;
                }).catch(function (errors){
                    $scope.loading = false;
                });
        };


        // @TODO : If statements...
        $scope.calculateClosestAddress = function($current_location_latitude, $current_location_longitude){
            $scope.current_location_latitude = $current_location_latitude;
            $scope.current_location_longitude = $current_location_longitude;

            angular.forEach($scope.data, function(value, key) {
                    $scope.data[key].distance = $scope.calculateDistance($scope.current_location_latitude,
                                                                         $scope.current_location_longitude,
                                                                         value.customer.latitude,
                                                                         value.customer.longitude);
            });
            $scope.data = $filter('orderBy')($scope.data, '+distance');
            console.log($scope.data);
        };

        // @TODO only request affected object
        $scope.changeStatus = function($route_manager_id, $status, $event){
            $event.stopPropagation();
            FacturatieRouteManager.changeStatus($route_manager_id, $status)
                .then(function (success){
                    $scope.loadRouteData($scope.day, $scope.week_nr, $scope.year);
                    $scope.loading = false;
                }).catch(function (errors){
                    $scope.loading = false;
                });
        };

        $scope.betaald = function($status){
            if($status == 'Onbetaald'){
                return false;
            } else if($status == 'Betaald'){
                return true;
            }
        };

        $scope.checkIndex = function($my_index, $index){
            if($my_index == $index){
                return true;
            } else {
                return false;
            }
        };

        $scope.nextAddress = function($current_location_latitude, $current_location_longitude, $my_index, $index){
            // @TODO : Only affect addresses to come not previous addresses in array
            $scope.calculateClosestAddress($current_location_latitude, $current_location_longitude);
            $scope.my_index++;
        };

        $scope.previousAddress = function($my_index, $index){
            $scope.my_index--;
        };

    });


