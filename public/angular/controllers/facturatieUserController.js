angular.module('facturatieUserCtrl', [])

// inject the Comment service into our controller
    .controller('facturatieUserController', function($scope, $http, $timeout, FacturatieUser) {

        $scope.loading = true;
        $scope.once = true;
        $scope.isSaving = false;

        $scope.object = {};
        $scope.types = [{
            Id: 1,
            Name: 'Admin'
        }, {
            Id: 2,
            Name: 'Bezorger'
        }];

        /**
         * Initialize the overview page
         */
        $scope.init = function() {
            FacturatieUser.getAllWithPagination(1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.get = function($id){
            FacturatieUser.get($id)
                .then(function (success) {
                    $scope.object = success.data;
                    if($scope.object.type == 'admin'){
                        $scope.selected_type = $scope.types[0];
                    } else if($scope.object.type == 'bezorger'){
                        $scope.selected_type = $scope.types[1];
                    }
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get data by pagination - 10 item per page
         * @param $page : the page you want the data from
         */
        $scope.getPage = function($page) {
            $scope.loading = true;
            FacturatieUser.getAllWithPagination($page)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get data for the next page [the next 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         * @param $last_page : is het last page of the data set
         */
        $scope.nextPage = function($current_page, $last_page) {
            if($current_page != $last_page) {
                $scope.loading = true;
                FacturatieUser.getAllWithPagination($current_page + 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Get data for the previous page [the previous 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         */
        $scope.previousPage = function($current_page) {
            if($current_page != 1) {
                $scope.loading = true;
                FacturatieUser.getAllWithPagination($current_page - 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Search for data by string.
         * If data matches part of the string one or more results will be returned
         * Else if $search_value string is empty it will return the first page of the current data set
         *
         * @param $search_value : is the given input as string
         */
        $scope.search = function($search_value) {
            if($search_value != ''){
                $scope.loading = true;
                FacturatieUser.search($search_value)
                    .then(function (success) {
                        $scope.data = success;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                FacturatieUser.getAllWithPagination(1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Deletes the given object by id
         * Return to the current page after delete
         *
         * @param $id : id of the object
         * @param $current_page : the page the dataset is on
         */
        $scope.delete = function($id){
            $scope.loading = true;
            FacturatieUser.delete($id)
                .then(function (success) {
                    FacturatieUser.getAllWithPagination(success.current_page)
                        .then(function (success) {
                            $scope.data = success.data;
                        }).catch(function (e) {
                            console.log("got an error in the process", e);
                        });
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.addObject = function(){
            $scope.isSaving = true;
            $scope.validationMessage = false;

            $scope.object.type = $scope.selected_type.Name;

            FacturatieUser.addObject($scope.object)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een gebruiker aangemaakt.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        $scope.editObject = function($id){
            $scope.isSaving = true;
            $scope.validationMessage = false;

            $scope.object.type = $scope.selected_type.Name;

            FacturatieUser.editObject($scope.object, $id)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een gebruiker gewijzigd.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        $scope.GetValueTypeDropdown = function () {
            var typeID = this.selected_type.Id;
            $scope.selected_type = $.grep($scope.types, function (selected_type) {
                return selected_type.Id == typeID;
            })[0];
            return $scope.selected_type;
        };

    });


