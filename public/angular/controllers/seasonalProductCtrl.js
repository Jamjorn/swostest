angular.module('seasonalProductCtrl', [])

    .controller('seasonalProductController', function($scope, $http, $timeout, $window, Seasonalproducts) {

        $scope.loading = true;
        $scope.once = true;
        $scope.isSaving = false;

        $scope.object = {};
        $scope.selected_type = '';
        $scope.selected_taart = '';

        $scope.selected_seasons = '';
        $scope.seasons = [];


        var select2 = this;
        select2.multipleDemo = {};

        $scope.types = [{
            Id: 1,
            Name: 'standaard'
        }, {
            Id: 2,
            Name: 'brood'
        }, {
            Id: 3,
            Name: 'taart'
        }];

        $scope.taarten = [{
            Id: 1,
            Name: 'beide'
        }, {
            Id: 2,
            Name: 'vulling'
        }, {
            Id: 3,
            Name: 'buitenkant'
        }];

        /**
         * Initialize the overview page
         */
        $scope.init = function() {
            Seasonalproducts.getAllWithPagination(1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        /**
         * Initialize the toevoegen page
         */
        $scope.initMainCategories = function() {
                   Seasonalproducts.getAllSeasons()
                .then(function (success) {
                    angular.forEach(success.data, function(value, key, key, key) {
                        $scope.seasons.push({ Id: value.id, seasonName: value.seasons[0].name , seasonID: value.seasons[0].id, category: value.categories[0].name});
                    });
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });



        };
        

        $scope.GetSeasonsDropdown = function () {
            var seasonsID = this.selected_seasons.Id;
            $scope.selected_seasons = $.grep($scope.seasons, function (selected_seasons) {
                return selected_seasons.Id == seasonsID;
            })[0];
            return $scope.selected_seasons;
        };

        $scope.get = function($id){
            Seasonalproducts.get($id)
                .then(function (success) {
                    $scope.object = success.data;
                    $scope.object.productName = success.data.name;
                    $scope.object.singleImage = success.data.path;
                    for ($i = 0; $i < $scope.types.length; $i++) {
                        if ($scope.object.type == $scope.types[$i].Name) {
                            $scope.selected_type = $scope.types[$i];
                        }
                    }
                    for ($i = 0; $i < $scope.taarten.length; $i++) {
                        if ($scope.object.soort_taart == $scope.taarten[$i].Name) {
                            $scope.selected_taart = $scope.taarten[$i];
                        }
                    }
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get data by pagination - 10 item per page
         * @param $page : the page you want the data from
         */
        $scope.getPage = function($page) {
            $scope.loading = true;
            Seasonalproducts.getAllWithPagination($page)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get data for the next page [the next 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         * @param $last_page : is het last page of the data set
         */
        $scope.nextPage = function($current_page, $last_page) {
            if($current_page != $last_page) {
                $scope.loading = true;
                Seasonalproducts.getAllWithPagination($current_page + 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Get data for the previous page [the previous 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         */
        $scope.previousPage = function($current_page) {
            if($current_page != 1) {
                $scope.loading = true;
                Seasonalproducts.getAllWithPagination($current_page - 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Search for data by string.
         * If data matches part of the string one or more results will be returned
         * Else if $search_value string is empty it will return the first page of the current data set
         *
         * @param $search_value : is the given input as string
         */
        $scope.search = function($search_value) {
            if($search_value != ''){
                $scope.loading = true;
                Seasonalproducts.search($search_value)
                    .then(function (success) {
                        $scope.data = success;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                Seasonalproducts.getAllWithPagination(1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Deletes the given object by id
         * Return to the current page after delete
         *
         * @param $id : id of the object
         * @param $current_page : the page the dataset is on
         */
        $scope.delete = function($id){
            $scope.loading = true;
            Seasonalproducts.delete($id)
                .then(function (success) {
                    Seasonalproducts.getAllWithPagination(success.current_page)
                        .then(function (success) {
                            $scope.data = success.data;
                        }).catch(function (e) {
                            console.log("got an error in the process", e);
                        });
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.addObject = function(){
            $scope.isSaving = true;
            $scope.validationMessage = false;
            $scope.object.season = $scope.selected_seasons.Name;
            $scope.object.type = $scope.selected_type.Name;
            if($scope.object.type == 'standaard'){
                $scope.object.soort_taart = "";
                $scope.object.price_half_bread = "";
            } else if($scope.object.type == 'brood'){
                $scope.object.soort_taart = "";
            } else if($scope.object.type == 'taart'){
                $scope.object.price_half_bread = "";
                $scope.object.soort_taart = $scope.selected_taart.Name;
            }
            var summernote = angular.element('.note-editor .panel-body').html().trim();
            if(summernote == '<p><br></p>'){ summernote = ''; }
            $scope.object.description = summernote;

            Seasonalproducts.addObject($scope.object)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een product aangemaakt.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        $scope.editObject = function($id){
            $scope.isSaving = true;
            $scope.validationMessage = false;

            $scope.object.type = $scope.selected_type.Name;
            if($scope.object.type == 'standaard'){
                $scope.object.soort_taart = "";
                $scope.object.price_half_bread = "";
            } else if($scope.object.type == 'brood'){
                $scope.object.soort_taart = "";
            } else if($scope.object.type == 'taart'){
                $scope.object.price_half_bread = "";
                $scope.object.soort_taart = $scope.selected_taart.Name;
            }
            var summernote = angular.element('.note-editor .panel-body').html().trim();
            if(summernote == '<p><br></p>'){ summernote = ''; }
            $scope.object.description = summernote;

            Seasonalproducts.editObject($scope.object, $id)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een product gewijzigd.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        /**
         * Get Single Product By ID
         * @param $id : Long
         */
        $scope.getInWebshop = function($id) {
            $scope.loading = true;
            Seasonalproducts.get($id)
                .then(function(data) {
                    $scope.singleProduct = data.data;
                    $scope.loading = false;
                });
        };

        /**
         * Get all Products by Category
         * @param $category : Category name in which the products belong
         */
        $scope.getAllInWebshop = function($category) {
            $scope.loading = true;
            Seasonalproducts.getAll($category)
                .then(function(data) {
                    $scope.data = data.data;
                    $scope.loading = false;
                });
        };

        /**
         * Search All Products by Input
         * @param $input : String
         */
        $scope.searchInWebshop = function ($input)
        {
            $scope.loading = true;
            if($input != ''){
                Seasonalproducts.search($input)
                    .then(function(data) {
                        $scope.data = data.data;
                        $scope.loading = false;
                    });
            } else {
                var active_category = $(".active-cat");
                active_category = $.trim(active_category.text());
                Seasonalproducts.getAll(active_category)
                    .then(function(data) {
                        $scope.data = data.data;
                        $scope.loading = false;
                    });
            }
        };

        $scope.initDropdown = function($id){
            if($scope.once) {
                $scope.loading = true;
                Seasonalproducts.get($id)
                    .then(function (success) {
                        $scope.data = success.data;
                        for ($i = 0; $i < $scope.types.length; $i++) {
                            if ($scope.data.type == $scope.types[$i].Name) {
                                $scope.selected_type = $scope.types[$i];
                            }
                        }
                        for ($i = 0; $i < $scope.taarten.length; $i++) {
                            if ($scope.data.soort_taart == $scope.taarten[$i].Name) {
                                $scope.selected_taart = $scope.taarten[$i];
                            }
                        }
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
            $scope.once = false;
        };

        $scope.$watch('selected_type', function(newVal, oldVal) {
            $scope.standaard = false;
            $scope.brood = false;
            $scope.taart = false;

            if(newVal.Name == 'standaard'){
                $scope.standaard = true;
            } else if(newVal.Name == 'brood'){
                $scope.brood = true;
            } else if(newVal.Name == 'taart'){
                $scope.taart = true;
                $scope.selected_taart = $scope.taarten[0];
                $scope.GetValueDropdown(1); // Initialize Dropdown
            }
        });

        $scope.GetValueDropdown = function () {
            var taartID = this.selected_taart.Id;
            $scope.selected_taart = $.grep($scope.taarten, function (selected_taart) {
                return selected_taart.Id == taartID;
            })[0];
            return $scope.selected_taart;
        };


        $scope.getCategories = function(){

            Seasonalproducts.getAllWithProducts()
            .then(function (success) {
                    $scope.data = success.data;
                    select2.listItem = $scope.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

    $scope.someGroupFn = function (item){
        if (item.name[0] >= 'A' && item.name[0] <= 'M')
                return 'From A - M';
        
        if (item.name[0] >= 'N' && item.name[0] <= 'Z')
            return 'From N - Z';
    };

    });


