angular.module('taartCtrl', [])

    .controller('taartController', function($scope, $http) {

        $scope.textTaartLength = 100;

        $scope.$watch('selected_tekst_op_taart', function (newValue, oldValue) {
            if (newValue) {
                if (newValue.length > 100) {
                    $scope.selected_tekst_op_taart = oldValue;
                }
                $scope.textTaartLength = 100 - newValue.length;
            }
        });

        $scope.updateBody = function (event) {
            return false;
        };

        $scope.openTaartPopup = function($id){

            // Load CSS
            $("body,html").css("overflow-y", "hidden");
            $("#taart-product-popup").css("display", "block");
            $(".overlay-webshop").css("display", "block");

            $scope.get($id);

        };

        $scope.closeTaartSelection = function() {
            $("body,html").css("overflow-y", "scroll");
            $(".overlay-webshop").css("display", "none");
            $("#taart-product-popup").css("display", "none");
        };

        $('.overlay-webshop').click(function() {
            $scope.closeTaartSelection();
        });

        $scope.firstStep = function($shape) {

            // reset CSS state
            $(".buitenkant").css("background-color", "#e5e5e5");
            $(".buitenkant-vorm").css({"background-color" : "#e5e5e5", "color" : "#000000", "border" : "1px solid #d6d6d6" });
            $(".round-taart").css("background-color", "#cfcfcf");
            $(".square-taart").css("background-color", "#cfcfcf");

            // set active CSS state
            if($shape == "rond"){
                $("#rond").css("background-color", "#c7387a");
                $("#rond > .buitenkant-vorm").css({"background-color" : "#c7387a", "color" : "#FFFFFF", "border" : "1px solid #c7387a"});
                $("#rond > .round-taart").css("background-color", "#FFFFFF");
            } else if($shape == "vierkant"){
                $("#vierkant").css("background-color", "#c7387a");
                $("#vierkant > .buitenkant-vorm").css({"background-color" : "#c7387a", "color" : "#FFFFFF", "border" : "1px solid #c7387a"});
                $("#vierkant > .square-taart").css("background-color", "#FFFFFF");
            }

            // set views
            $scope.selected_none = true;
            $scope.selected_vorm = true;
            if($shape == "rond"){
                $(".aantal-personen .option").css("background-color", "#e5e5e5");
                $(".aantal-personen .option").css("color", "#000");
                $scope.selected_personen_value = '';
                $scope.personen_vierkant = false;
                $scope.personen_rond = true;
                $scope.prijs_for_taart = 0;
            } else if($shape == "vierkant"){
                $(".aantal-personen .option").css("background-color", "#e5e5e5");
                $(".aantal-personen .option").css("color", "#000");
                $scope.selected_personen_value = '';
                $scope.personen_rond = false;
                $scope.personen_vierkant = true;
                $scope.prijs_for_taart = 0;
            }

            // set values
            $scope.selected_vorm_value = $shape;

        };

        $scope.secondStep = function($nr_of_people, $price, $event) {

            // reset CSS state
            $("#taart-product-popup .aantal-personen .option").css("background-color", "#e5e5e5");
            $("#taart-product-popup .aantal-personen .option").css("color", "#000000");
            $("#taart-product-popup .aantal-personen .option").find(".price").css("color", "#000000");

            // set active CSS state
            $($event.currentTarget).css("background-color", "#c7387a");
            $($event.currentTarget).css("color", "#FFFFFF");
            $($event.currentTarget).find(".price").css("color", "#000000");

            // set views
            $scope.tekst_op_taart_vraag = true;
            $scope.selected_personen = true;
            $scope.prijs_for_taart = true;

            // set values
            $scope.selected_personen_value = $nr_of_people;
            $scope.nr_of_people = $nr_of_people;

            // parse values
            $nr_of_people = parseFloat($nr_of_people, 6);
            $price = parseFloat($price, 6);

            // set price value
            if($scope.selected_gesneden_value == 'Nee') {
                $scope.prijs_for_taart_value = parseFloat($price * $nr_of_people, 6);
            } else if($scope.selected_gesneden_value == 'Ja'){
                $scope.prijs_for_taart_value = parseFloat(($price * $nr_of_people) + 1.25, 6);
            } else {
                $scope.prijs_for_taart_value = parseFloat($price * $nr_of_people, 6);
            }

        };

        $scope.thirdStep = function($tekst_vraag, $event) {

            // reset CSS state
            $("#taart-product-popup .tekst-op-taart-vraag .option").css("background-color", "#e5e5e5");
            $("#taart-product-popup .tekst-op-taart-vraag .option").css("color", "#000000");
            $("#taart-product-popup .tekst-op-taart-vraag .option").find(".price").css("color", "#000000");

            // set active CSS state
            $($event.currentTarget).css("background-color", "#c7387a");
            $($event.currentTarget).css("color", "#FFFFFF");

            // set views
            if ($tekst_vraag == 'Ja') {
                $scope.tekst_op_taart = true;
                $scope.gesneden = true;
            } else if ($tekst_vraag == 'Nee'){
                $scope.tekst_op_taart = false;
                $scope.gesneden = true;
            }
            $scope.selected_tekst_vraag = true;

            // set values
            $scope.selected_tekst_vraag_value = $tekst_vraag;

        };

        $scope.fourthStep = function($gesneden, $price, $event) {

            // reset CSS state
            $("#taart-product-popup .gesneden .option").css("background-color", "#e5e5e5");
            $("#taart-product-popup .gesneden .option").css("color", "#000000");
            $("#taart-product-popup .gesneden .option").find(".price").css("color", "#000000");

            // set active CSS state
            $($event.currentTarget).css("background-color", "#c7387a");
            $($event.currentTarget).css("color", "#FFFFFF");
            $($event.currentTarget).find(".price").css("color", "#000000");

            // set views
            $scope.buitenkant = true;
            $scope.selected_gesneden = true;

            // set values
            $scope.selected_gesneden_value = $gesneden;

            // parse values
            $price = parseFloat($price, 6);
            $scope.nr_of_people = parseFloat($scope.nr_of_people, 6);

            // set price value
            if($gesneden == 'Ja'){
                $scope.prijs_for_taart_value = parseFloat(($price * $scope.nr_of_people) + 1.25, 6);
            } else if($gesneden == 'Nee'){
                $scope.prijs_for_taart_value = parseFloat($price * $scope.nr_of_people, 6);
            }


        };

        $scope.fifthStep = function($buitenkant, $event) {

            // reset CSS state
            $(".buitenkant").css("border", "none");

            // set active CSS state
            $($event.currentTarget).css("border", "3px solid #c7387a");

            // set views
            $scope.vulling = true;
            $scope.selected_buitenkant = true;

            // set values
            $scope.selected_buitenkant_value = $buitenkant;

        };

        $scope.sixthStep = function($buitenkant, $event) {

            // reset CSS state
            $("#taart-product-popup .vulling .option").css("background-color", "#e5e5e5");
            $("#taart-product-popup .vulling .option").css("color", "#000000");
            $("#taart-product-popup .vulling .option").find(".price").css("color", "#000000");

            // set active CSS state
            $($event.currentTarget).css("background-color", "#c7387a");
            $($event.currentTarget).css("color", "#FFFFFF");

            // set views
            $scope.selected_vulling = true;
            $scope.finished_taart_selection = true;

            // set values
            $scope.selected_vulling_value = $buitenkant;

        };

    });


