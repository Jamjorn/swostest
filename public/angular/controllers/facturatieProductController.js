angular.module('facturatieProductCtrl', [])

// inject the Comment service into our controller
    .controller('facturatieProductController', function($scope, $http, $timeout, FacturatieProduct, FacturatieCategory) {

        $scope.loading = true;
        $scope.once = true;
        $scope.isSaving = false;

        $scope.object = {};
        var select2 = this;
        select2.multipleDemo = {};

        /**
         * Initialize the overview page
         */
        $scope.init = function() {
            FacturatieProduct.getAllWithPagination(1)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.get = function($id){
            $scope.getCategories();
            FacturatieProduct.get($id)
                .then(function (success) {
                    $scope.object = success.data;
                    select2.multipleDemo.removeSelectIsFalse = $scope.object.categories;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get data by pagination - 10 item per page
         * @param $page : the page you want the data from
         */
        $scope.getPage = function($page) {
            $scope.loading = true;
            FacturatieProduct.getAllWithPagination($page)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get data for the next page [the next 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         * @param $last_page : is het last page of the data set
         */
        $scope.nextPage = function($current_page, $last_page) {
            if($current_page != $last_page) {
                $scope.loading = true;
                FacturatieProduct.getAllWithPagination($current_page + 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Get data for the previous page [the previous 10 objects]
         *
         * @param $current_page : is the current page the data is displayed
         */
        $scope.previousPage = function($current_page) {
            if($current_page != 1) {
                $scope.loading = true;
                FacturatieProduct.getAllWithPagination($current_page - 1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Search for data by string.
         * If data matches part of the string one or more results will be returned
         * Else if $search_value string is empty it will return the first page of the current data set
         *
         * @param $search_value : is the given input as string
         */
        $scope.search = function($search_value) {
            if($search_value != ''){
                $scope.loading = true;
                FacturatieProduct.search($search_value)
                    .then(function (success) {
                        $scope.data = success;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                FacturatieProduct.getAllWithPagination(1)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            }
        };

        /**
         * Deletes the given object by id
         * Return to the current page after delete
         *
         * @param $id : id of the object
         * @param $current_page : the page the dataset is on
         */
        $scope.delete = function($id){
            $scope.loading = true;
            FacturatieProduct.delete($id)
                .then(function (success) {
                    FacturatieProduct.getAllWithPagination(success.current_page)
                        .then(function (success) {
                            $scope.data = success.data;
                        }).catch(function (e) {
                            console.log("got an error in the process", e);
                        });
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        $scope.addObject = function(){
            $scope.isSaving = true;
            $scope.validationMessage = false;

            if(select2.hasOwnProperty('multipleDemo')){
                $scope.object.categories = select2.multipleDemo.removeSelectIsFalse;
            } else {
                $scope.object.categories = '';
            }

            FacturatieProduct.addObject($scope.object)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een product aangemaakt.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        $scope.editObject = function($id){
            $scope.isSaving = true;
            $scope.validationMessage = false;

            if(select2.hasOwnProperty('multipleDemo')){
                $scope.object.categories = select2.multipleDemo.removeSelectIsFalse;
            } else {
                $scope.object.categories = '';
            }

            FacturatieProduct.editObject($scope.object, $id)
                .then(function (success){
                    resetValidationErrors();
                    $scope.validationMessage = 'U heeft succesvol een product gewijzigd.';
                    $timeout( function(){
                        $scope.isSaving = false;
                        angular.element('.flash-message').slideUp(300);
                    }, 3000);
                }).catch(function (errors){
                    showValidationErrors(errors);
                    $scope.isSaving = false;
                });
            $scope.loading = false;
        };

        $scope.getCategories = function(){

            FacturatieCategory.getAll()
                .then(function (success) {
                    $scope.data = success.data;
                    select2.listItem = $scope.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

    });


