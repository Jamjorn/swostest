angular.module('timeCtrl', [])

    .controller('TimeController', function($scope, $filter, $http, Times) {

        moment.locale('nl');
        $scope.i = 0;

        $scope.get = function($location, $type){
            $scope.loading = true;
            $scope.timeschedule = true;
            $scope.location = $location;
            $scope.type = $type;
            Times.get($location, $type)
                .then(function(data) {
                    $scope.vacation_1 = $filter('between')($scope.getMomentDate(0), data.data[0].vacations);
                    $scope.vacation_2 = $filter('between')($scope.getMomentDate(1), data.data[0].vacations);
                    $scope.vacation_3 = $filter('between')($scope.getMomentDate(2), data.data[0].vacations);
                    $scope.vacation_4 = $filter('between')($scope.getMomentDate(3), data.data[0].vacations);
                    $scope.vacation_5 = $filter('between')($scope.getMomentDate(4), data.data[0].vacations);
                    $scope.vacation_6 = $filter('between')($scope.getMomentDate(5), data.data[0].vacations);
                    $scope.vacation_7 = $filter('between')($scope.getMomentDate(6), data.data[0].vacations);

                    $scope.column_1 = $scope.getTimescheduleColumn(1, $scope.vacation_1, data.data[0].timeblocks);
                    $scope.column_2 = $scope.getTimescheduleColumn(2, $scope.vacation_2, data.data[0].timeblocks);
                    $scope.column_3 = $scope.getTimescheduleColumn(3, $scope.vacation_3, data.data[0].timeblocks);
                    $scope.column_4 = $scope.getTimescheduleColumn(4, $scope.vacation_4, data.data[0].timeblocks);
                    $scope.column_5 = $scope.getTimescheduleColumn(5, $scope.vacation_5, data.data[0].timeblocks);
                    $scope.column_6 = $scope.getTimescheduleColumn(6, $scope.vacation_6, data.data[0].timeblocks);
                    $scope.column_7 = $scope.getTimescheduleColumn(7, $scope.vacation_7, data.data[0].timeblocks);

                    $scope.loading = false;
                });
        };

        $scope.getTimescheduleColumn = function (columnNumber, vacation, timeblocks){
            var column;
            if(!vacation.length) {
                if(columnNumber == 1){
                    if ($scope.i > 0) {
                        column = $filter('onDay')($filter('capitalize')($scope.getDay($scope.i)), timeblocks);
                    } else {
                        column = '';
                    }
                } else if(columnNumber == 2){
                    if ($scope.i == 0 && moment().format("HH:mm") > "16:00") {
                        column = '';
                    } else if ($scope.i >= 0) {
                        column = $filter('onDay')($filter('capitalize')($scope.getDay($scope.i + (columnNumber - 1))), timeblocks);
                    }
                } else {
                    column = $filter('onDay')($filter('capitalize')($scope.getDay($scope.i + (columnNumber - 1))), timeblocks);
                }
            }
            return column;
        };

        $scope.getDay = function($days_forward) {
            return $filter('capitalize')(moment().add($days_forward + $scope.i, 'days').format("dddd"));
        };

        $scope.getDate = function($days_forward) {
            return moment().add($days_forward + $scope.i, 'days').format("DD-MM-YYYY");
        };

        $scope.getMomentDate = function($days_forward){
            return moment().add($days_forward + $scope.i, 'days');
        };

        $scope.nextWeek = function(){
            $scope.i = $scope.i + 7;
            $scope.get($scope.location, $scope.type);
        };

        $scope.previousWeek = function(){
            if($scope.i > 0) {
                $scope.i = $scope.i - 7;
                $scope.get($scope.location, $scope.type);
            }
        };

        $scope.selectedTimeAndDate = function ($event, $time, $date) {
            var button3 = angular.element('.button-3-series');
            button3.css('color', '#000');
            button3.css('background-color', '#e5e5e5');
            var elem = angular.element($event.currentTarget);
            elem.css('color', '#FFF');
            elem.css('background-color', '#c7387a');

            $scope.timeChosen = moment($time.begintime).format("HH:mm") + " - " + moment($time.endtime).format("HH:mm");
            $scope.dateChosen = $date;

            $scope.timeschedule = false;
            $scope.keuze_overzicht = true;
        };

        $scope.loadLastStepOrder = function(){
            $scope.keuze_overzicht = false;
            $scope.keuze_betaal_overzicht = true;
        };

        $scope.previous = function(){
            $scope.timeschedule = false;
            $scope.i = 0;
            if($scope.ophalen == true) {
                $scope.ophalen_selected = true;
            } else if($scope.bezorgen == true){
                $scope.bezorgen_selected = true;
            }
        };

        $scope.getCustomer = function(){
            $scope.loading = true;
            Times.getCustomer()
                .then(function(data) {
                    $scope.customer = data.data;
                    $scope.customer.fullName = $filter('fullName')($scope.customer.firstname, $scope.customer.insertion, $scope.customer.lastname);
                    $scope.loading = false;
                });
        };

    });
