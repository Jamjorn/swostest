angular.module('pasenCtrl', [])

// inject the Comment service into our controller
    .controller('pasenController', function($scope, $http) {

        $scope.textProductNaamLength = 50;
        $scope.items = [];
        $scope.lastpage=1;
        $scope.thelastpage = 0;
        $scope.kerst_products = '';
        $scope.deleteURL = '/admin/pasen/delete/';
        $scope.loading = true;

        $scope.$watch('naam', function (newValue, oldValue) {
            if (newValue) {
                if (newValue.length > 50) {
                    $scope.naam = oldValue;
                }
                $scope.textProductNaamLength = 50 - newValue.length;
            }
        });

        $scope.updateBody = function (event) {
            return false;
        };

        $scope.init = function() {
            $scope.lastpage=1;
            $http({
                url: 'api/pasen',
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function(data, status, headers, config) {
                $scope.thelastpage = data.last_page;
                $scope.data = data.data;
                $scope.currentpage = data.current_page;
                $scope.loading = false;
            });
        };

        $scope.init();

        $scope.getFirstPage = function() {
            $scope.loading = true;
            $scope.lastpage = 1;
            $http({
                url: 'api/pasen',
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function (data, status, headers, config) {
                $scope.kerst_products = data.data;
                $scope.loading = false;
            });
        };

        $scope.getLastPage = function() {
            $scope.loading = true;
            $scope.lastpage = $scope.thelastpage;
            $http({
                url: 'api/pasen',
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function (data, status, headers, config) {
                $scope.kerst_products = data.data;
                $scope.loading = false;
            });
        };

        $scope.nextPage = function() {
            $scope.loading = true;
            $scope.lastpage +=1;
            $http({
                url: 'api/pasen',
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function (data, status, headers, config) {
                $scope.kerst_products = data.data;
                $scope.loading = false;
            });
        };

        $scope.previousPage = function() {
            $scope.loading = true;
            if($scope.lastpage == 1)
            {
                $scope.lastpage=1;
            } else {
                $scope.lastpage -=1;
            }
            $http({
                url: 'api/pasen',
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function (data, status, headers, config) {
                $scope.kerst_products = data.data;
                $scope.loading = false;
            });
        };

        $scope.reloadKerstItems = function() {
            $scope.loading = true;
            $http({
                url: 'api/pasen',
                method: "GET",
                params: {page:  $scope.lastpage}
            }).then(function (data, status, headers, config) {
                $scope.kerst_products = data.data;
                $scope.loading = false;
            });
        };

        $scope.deleteKerstItems = function($id) {
            $scope.loading = true;
            $http({
                method  : 'POST',
                url     : 'pasen/delete/' + $id
            }).then(function(data) {
                $scope.reloadKerstItems();
                $scope.loading = false;
            });

        };

        $scope.searchKerstItems = function() {
            if($scope.search_value != ''){
                $scope.loading = true;
                $http({
                    method  : 'POST',
                    url     : 'pasen/search/' + $scope.search_value
                }).then(function(data) {
                    $scope.kerst_products = data;
                    $scope.loading = false;
                });
            } else {
                $scope.init();
            }
        };

    });


